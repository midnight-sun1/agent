#pragma once
#include <algorithm>
#include <array>
#include <cmath>
#include <map>
#include <numeric>
#include <random>
#include <ranges>
#include <utility>
#include <vector>
#include "SFML/Graphics.hpp"
#include "base.hpp"
#include "concurrentqueue.h"
#include "Eigen/Dense"
#include "magic_enum.hpp"

namespace Catch
{
struct Player
{
    Eigen::Vector2<int32> pos;
    Eigen::Vector2<int32> pos_d;
    int32 color;
    int32 color_original;
    int32 jumps;
    int32 timeout;
};

struct Entity
{
    enum class Type : uint8
    {
        NONE,
        WALL,
        PLAYER,
    } type;
    Player *player;
};

template <int32 Players, int32 HammerStrength, int32 ViewRange, typename Scalar>
struct State
{
    using Color = std::array<uint8, 3>;
    using ScalarT = Scalar;

    static constexpr int32 ViewSize = ViewRange * 2 + 1;
    static constexpr size_t Length = ViewSize * ViewSize * (magic_enum::enum_count<Entity::Type>() + Players - 2) + 2;
    static constexpr int32 HammerStrengthV = HammerStrength;

    Eigen::Matrix<Entity, ViewSize, ViewSize> view;
    static constexpr std::array<Color, Players> colors = []() constexpr
    {
        auto hash = [](uint32 x) constexpr
        {
            x = ((x >> 16) ^ x) * 0x45d9f3b;
            x = ((x >> 16) ^ x) * 0x45d9f3b;
            x = (x >> 16) ^ x;
            return x;
        };

        std::array<Color, Players> colors_t;
        auto hash_t = hash(2137);

        for (auto &&[index, color] : std::views::zip(std::views::iota(0, Players), colors_t))
        {
            color[index % 3] = 255;
            hash_t = hash(hash_t);
            color[(index % 3 + 1) % 3] = static_cast<uint8>(hash_t % 128 + 64);
            hash_t = hash(hash_t);
            color[(index % 3 + 2) % 3] = static_cast<uint8>(hash_t % 256);
        }
        return colors_t;
    }();

    sf::VertexArray *DrawData() const
    {
        auto vertices = new sf::VertexArray();
        vertices->setPrimitiveType(sf::Quads);
        const float32 scale = 1 / (2 * static_cast<float32>(ViewRange) + 1);

        for (int32 y = 0; y < view.cols(); y++)
            for (int32 x = 0; x < view.rows(); x++)
            {
                std::array<uint8, 3> color = {0, 0, 0};
                switch (view(x, y).type)
                {
                case Entity::Type::NONE:
                    continue;

                case Entity::Type::WALL:
                    color = {255, 255, 255};
                    break;

                case Entity::Type::PLAYER:
                    color = colors[view(x, y).player->color];
                    break;
                }

                for (auto &&[dx, dy] : {std::array<float32, 2>{0, 0}, {0, 1}, {1, 1}, {1, 0}})
                {
                    vertices->append({{(x + dx) * scale, (y + dy) * scale}, {color[0], color[1], color[2]}});
                }
            }
        return vertices;
    }

    const Eigen::Vector<Scalar, Length> ToVec() const
    {
        auto vec = Eigen::Vector<Scalar, Length>();
        for (auto &v : vec)
            v = 0;

        int32 i = 0;
        for (auto &e : view.reshaped())
        {
            if (e.type == Entity::Type::NONE)
            {
                i += magic_enum::enum_count<Entity::Type>() - 2 + Players;
                continue;
            }
            else if (e.type == Entity::Type::WALL)
                vec[i] = 1;
            else
                vec[i + 1 + e.player->color] = 1;

            i += magic_enum::enum_count<Entity::Type>() - 2 + Players;
        }
        auto &player = view(ViewRange, ViewRange).player;
        vec[i++] = player->pos_d[0];
        vec[i] = player->pos_d[1];

        return vec;
    }
};

enum class MAction : uint8
{
    NONE,
    MOVE_UP,
    MOVE_DOWN,
    MOVE_LEFT,
    MOVE_RIGHT,
};

enum class HAction : uint8
{
    NONE,
    HIT,
    DASH,
    RECHARGE,
};

template <typename Scalar>
struct Move
{
    using ScalarT = Scalar;

    static constexpr size_t Length = magic_enum::enum_count<MAction>() + magic_enum::enum_count<HAction>();
    static constexpr size_t MDomain = magic_enum::enum_count<MAction>() * magic_enum::enum_count<HAction>();

    MAction m_action;
    HAction h_action;

    Move()
    {
        m_action = MAction::NONE;
        h_action = HAction::NONE;
    }

    Move(const MAction &m_action, const HAction &h_action) : m_action(m_action), h_action(h_action) {}

    Move(moodycamel::ConcurrentQueue<sf::Keyboard::Key> &keys)
    {
        sf::Keyboard::Key buff[10];
        size_t len = keys.try_dequeue_bulk(buff, 10);

        m_action = MAction::NONE;
        h_action = HAction::NONE;

        for (int32 i = 0; i < len; i++)
            switch (buff[i])
            {
            case sf::Keyboard::W:
                m_action = MAction::MOVE_UP;
                break;

            case sf::Keyboard::S:
                m_action = MAction::MOVE_DOWN;
                break;

            case sf::Keyboard::A:
                m_action = MAction::MOVE_LEFT;
                break;

            case sf::Keyboard::D:
                m_action = MAction::MOVE_RIGHT;
                break;

            case sf::Keyboard::Q:
                h_action = HAction::HIT;
                break;

            case sf::Keyboard::E:
                h_action = HAction::DASH;
                break;

            case sf::Keyboard::R:
                h_action = HAction::RECHARGE;
                break;
            }
    }

    Move(std::ranlux48_base *generator)
    {
        m_action = static_cast<MAction>(std::uniform_int_distribution<>(0, magic_enum::enum_count<MAction>() - 1)(*generator));
        h_action = static_cast<HAction>(std::uniform_int_distribution<>(0, magic_enum::enum_count<HAction>() - 1)(*generator));
    }

    Move(const Eigen::Vector<Scalar, Length> &vector)
    {
        m_action = static_cast<MAction>(std::distance(vector.begin(), std::max_element(vector.begin(), vector.begin() + magic_enum::enum_count<MAction>())));
        h_action = static_cast<HAction>(std::distance(vector.begin() + magic_enum::enum_count<MAction>(), std::max_element(vector.begin() + magic_enum::enum_count<MAction>(), vector.end())));
    }

    Move(const size_t &n)
    {
        m_action = static_cast<MAction>(n % magic_enum::enum_count<MAction>());
        h_action = static_cast<HAction>((n - size_t(m_action)) / magic_enum::enum_count<MAction>());
    }

    template <typename T>
    static Move Heuristic(std::ranlux48_base *generator, const T &state)
    {
        static constexpr int32 Center = (T::ViewSize - 1) / 2;
        std::tuple<int32, int32, int32> target = {0, 0, INT32_MAX};
        auto m_action = static_cast<MAction>(std::uniform_int_distribution<>(0, magic_enum::enum_count<MAction>() - 1)(*generator));
        auto h_action = HAction::NONE;

        auto search = [](const auto &view)
        {
            static constexpr int32 Center = (T::ViewSize - 1) / 2;
            auto dist = Eigen::Matrix<int32, T::ViewSize, T::ViewSize>();
            auto action = Eigen::Matrix<MAction, T::ViewSize, T::ViewSize>();
            std::fill(dist.reshaped().begin(), dist.reshaped().end(), INT32_MAX);
            std::queue<std::tuple<int32, int32, int32, MAction>> leafs;

            dist(Center, Center) = 0;
            leafs.push({Center, Center - 1, 0, MAction::MOVE_UP});
            leafs.push({Center, Center + 1, 0, MAction::MOVE_DOWN});
            leafs.push({Center - 1, Center, 0, MAction::MOVE_LEFT});
            leafs.push({Center + 1, Center, 0, MAction::MOVE_RIGHT});

            while (not leafs.empty())
            {
                auto [nx, ny, d, a] = leafs.front();
                leafs.pop();
                if (view(nx, ny).type != Entity::Type::NONE and dist(nx, ny) > d + 1)
                {
                    dist(nx, ny) = d + 1;
                    action(nx, ny) = a;
                    continue;
                }
                if (dist(nx, ny) < d + 1)
                    continue;

                dist(nx, ny) = d + 1;
                action(nx, ny) = a;

                if (ny - 1 >= 0)
                    leafs.push({nx, ny - 1, dist(nx, ny), a});
                if (ny + 1 < T::ViewSize)
                    leafs.push({nx, ny + 1, dist(nx, ny), a});
                if (nx - 1 >= 0)
                    leafs.push({nx - 1, ny, dist(nx, ny), a});
                if (nx + 1 < T::ViewSize)
                    leafs.push({nx + 1, ny, dist(nx, ny), a});
            }

            return std::tuple{dist, action};
        };

        auto [dist, action] = search(state.view);
        for (int32 y = 0; y < state.view.cols(); y++)
            for (int32 x = 0; x < state.view.rows(); x++)
                if (state.view(x, y).type == Entity::Type::PLAYER and state.view(x, y).player->color != state.view(Center, Center).player->color and dist(x, y) < std::get<2>(target))
                {
                    target = {x, y, dist(x, y)};
                    m_action = action(x, y);
                }

        if (std::abs(std::get<0>(target) - Center) <= T::HammerStrengthV and std::abs(std::get<1>(target) - Center) <= T::HammerStrengthV)
            h_action = HAction::HIT;

        return Move(m_action, h_action);
    }
};

template <
    int32 Players,
    int32 BoardRange = 32,
    int32 ViewRange = 6,
    typename Scalar = float32,
    int32 Timelimit = 1000,
    int32 Jumps = 3,
    int32 DashDistance = 8,
    int32 HammerStrength = 2,
    int32 RechargeTimeout = 8,
    float64 MazeDensity = 0.05f64,
    float64 ObstacleSize = 0.5f64,
    int32 MinSpawnpoints = Players>
requires requires { BoardRange >= ViewRange; }
class Game : public BaseGame<State<Players, HammerStrength, ViewRange, Scalar>, Move<Scalar>, Players>
{
    static constexpr int32 BoardSize = BoardRange * 2 + 1;
    using Board = Eigen::Matrix<Entity, BoardSize, BoardSize>;
    using Spawnpoint = std::array<int32, 2>;
    using Coord = std::array<int32, 2>;
    using Color = std::array<uint8, 3>;

    Board maze_template, world;
    std::vector<Spawnpoint> spawnpoints;
    std::array<Player, Players> players;
    std::map<int32, int32> colors;
    std::map<int32, int32> colors_top;

    int32 time;

    std::pair<Board, std::vector<Spawnpoint>> GenerateMaze()
    {
        Board blank;
        for (auto &&e : blank.reshaped())
            e = Entity{
                .type = Entity::Type::NONE,
            };

        std::set<Spawnpoint> spawnpoints;
        for (auto x : std::views::iota(0, BoardSize))
            for (auto y : std::views::iota(0, BoardSize))
                spawnpoints.insert({x, y});

        auto blank_copy = blank;
        auto spawnpoints_copy = spawnpoints;

        auto discrete_density = std::discrete_distribution<>{1 - MazeDensity, MazeDensity};
        auto discrete_obstacle = std::discrete_distribution<>{1 - ObstacleSize, ObstacleSize};
        auto perm_x = std::array<int32, BoardSize>();
        auto perm_y = std::array<int32, BoardSize>();
        std::ranges::iota(perm_x, 0);
        std::ranges::iota(perm_y, 0);
        std::ranges::shuffle(perm_x, *this->generator);
        std::ranges::shuffle(perm_y, *this->generator);

        for (auto x : perm_x)
            for (auto y : perm_y)
                if (discrete_density(*this->generator))
                {
                    auto size = std::array{1, 1};
                    while (discrete_obstacle(*this->generator) and size[0] <= BoardRange)
                        size[0]++;
                    while (discrete_obstacle(*this->generator) and size[1] <= BoardRange)
                        size[1]++;

                    for (auto dx : std::views::iota(0, size[0]))
                        for (auto dy : std::views::iota(0, size[1]))
                        {
                            blank((x + dx) % BoardSize, (y + dy) % BoardSize) = Entity{
                                .type = Entity::Type::WALL,
                            };
                            spawnpoints.erase({(x + dx) % BoardSize, (y + dy) % BoardSize});
                        }
                }

        if (spawnpoints.empty())
            return {blank_copy, std::vector<Spawnpoint>(spawnpoints_copy.begin(), spawnpoints_copy.end())};

        auto cmp = [](std::set<Spawnpoint> a, std::set<Spawnpoint> b) { return a.size() < b.size(); };
        auto mod = [](int32 i) constexpr { return (i % BoardSize + BoardSize) % BoardSize; };
        auto validated = std::set<std::set<Spawnpoint>, decltype(cmp)>();
        while (not spawnpoints.empty())
        {
            auto validating = std::set<Spawnpoint>();
            auto leafs = std::vector<Spawnpoint>();
            leafs.push_back(*spawnpoints.begin());
            spawnpoints.erase(spawnpoints.begin());

            while (not leafs.empty())
            {
                auto [x, y] = leafs.back();
                validating.insert({x, y});
                leafs.pop_back();
                for (auto [dx, dy] : {std::array<int32, 2>{0, 1}, {1, 0}, {0, -1}, {-1, 0}})
                {
                    auto [nx, ny] = std::tuple{mod(x + dx), mod(y + dy)};
                    if (spawnpoints.contains({nx, ny}))
                    {
                        leafs.push_back({nx, ny});
                        spawnpoints.erase({nx, ny});
                    }
                }
            }
            validated.insert(validating);
        }

        if (validated.rbegin()->size() < MinSpawnpoints)
            return {blank_copy, std::vector<Spawnpoint>(spawnpoints_copy.begin(), spawnpoints_copy.end())};

        return {blank, std::vector<Spawnpoint>((*validated.rbegin()).begin(), (*validated.rbegin()).end())};
    }

    bool Tick(const std::array<Move<Scalar>, Players> &moves)
    {
        auto indices = std::array<int32, Players>();
        std::iota(indices.begin(), indices.end(), 0);
        std::ranges::shuffle(indices, *this->generator);

        auto mod = [](int32 i) constexpr { return (i % BoardSize + BoardSize) % BoardSize; };

        for (auto &index : indices)
        {
            auto &&[player, move] = std::tie(players[index], moves[index]);
            auto &m_action = move.m_action;

            if (player.timeout or m_action == MAction::NONE)
                continue;

            auto &&[x, y] = std::tie(player.pos[0], player.pos[1]);
            auto &&[dx, dy] = std::tie(player.pos_d[0], player.pos_d[1]);
            auto &&[tx, ty] = std::map<MAction, Coord>{{MAction::MOVE_UP, {0, -1}}, {MAction::MOVE_DOWN, {0, 1}}, {MAction::MOVE_LEFT, {-1, 0}}, {MAction::MOVE_RIGHT, {1, 0}}}[m_action];
            auto &&[nx, ny] = std::tuple{mod(x + tx), mod(y + ty)};

            if (world(nx, ny).type == Entity::Type::NONE)
            {
                world(nx, ny) = world(x, y);
                world(x, y) = Entity{
                    .type = Entity::Type::NONE,
                };
                std::tie(dx, dy) = std::tuple{tx, ty};
                std::tie(x, y) = std::tuple{nx, ny};
            }
        }

        for (auto &index : indices)
        {
            auto &&[player, move] = std::tie(players[index], moves[index]);
            auto &h_action = move.h_action;

            if (player.timeout or h_action == HAction::NONE)
                continue;

            auto &&[x, y] = std::tie(player.pos[0], player.pos[1]);
            switch (h_action)
            {
            case HAction::HIT: // dear clang-format, wtf even is this?
            {
                for (const auto &p :
                     std::views::cartesian_product(std::views::iota(-HammerStrength, HammerStrength + 1), std::views::iota(-HammerStrength, HammerStrength + 1)) |
                         std::views::filter(
                             [](auto p)
                             {
                                 auto [tx, ty] = p;
                                 return tx != 0 or ty != 0;
                             }))
                {
                    const auto &[dx, dy] = p;
                    auto [nx, ny] = std::tuple{mod(x + dx), mod(y + dy)};
                    if (world(nx, ny).type == Entity::Type::PLAYER)
                    {
                        bool miss = false;
                        auto [sx, sy] = std::tuple{std::signbit(dx) ? -1 : 1, std::signbit(dy) ? -1 : 1};
                        for (int32 tx = 0; tx != dx + sx; tx += sx)
                            for (int32 ty = 0; ty != dy + sy; ty += sy)
                            {
                                if (tx == 0 and ty == 0)
                                    continue;
                                if (world(mod(x + tx), mod(y + ty)).type == Entity::Type::WALL)
                                    miss = true;
                            }

                        if (not miss)
                        {
                            auto &color = world(nx, ny).player->color;
                            colors[color]--;
                            colors[player.color]++;
                            colors_top[player.color] = std::max(colors_top[player.color], colors[player.color]);

                            if (not colors[color])
                                colors.erase(color);

                            color = player.color;
                        }
                    }
                }
            }
            break;
            case HAction::DASH:
            {
                if (not player.jumps)
                    continue;

                auto &&[dx, dy] = std::tie(player.pos_d[0], player.pos_d[1]);
                auto [nx, ny] = std::tuple{x, y};
                auto [ox, oy] = std::tuple{x, y};

                if (!dx and !dy)
                    continue;

                if (dx)
                    for (int32 tx = 0; tx <= DashDistance; tx++)
                    {
                        x = nx;
                        nx = mod(nx + dx);
                        if (world(nx, ny).type != Entity::Type::NONE)
                            break;
                    }
                else
                    for (int32 ty = 0; ty <= DashDistance; ty++)
                    {
                        y = ny;
                        ny = mod(ny + dy);
                        if (world(nx, ny).type != Entity::Type::NONE)
                            break;
                    }

                if (x == ox and y == oy)
                    continue;

                world(x, y) = world(ox, oy);
                world(ox, oy).type = Entity::Type::NONE;

                player.jumps--;
            }
            break;
            case HAction::RECHARGE:
            {
                player.timeout = RechargeTimeout;
                player.jumps = Jumps;
            }
            break;
            }
        }

        for (auto &player : players)
            if (player.timeout)
                player.timeout--;

        return not time-- or colors.size() == 1;
    }

    void Restart()
    {
        world = maze_template;
        colors.clear();
        colors_top.clear();
        std::ranges::shuffle(spawnpoints, *this->generator);

        for (auto &&[i, player, spawnpoint] : std::views::zip(std::views::iota((size_t)0, players.size()), players, spawnpoints))
        {
            auto &[x, y] = spawnpoint;
            player = Player{
                .pos = {x, y},
                .pos_d = {0, 0},
                .color = (int32)i,
                .color_original = (int32)i,
                .jumps = Jumps,
                .timeout = 0,
            };
            world(x, y) = Entity{
                .type = Entity::Type::PLAYER,
                .player = &player,
            };
            colors[i] = 1;
            colors_top[i] = 1;
        }
        time = Timelimit;
    }

    State<Players, HammerStrength, ViewRange, Scalar> GetState(int32 id)
    {
        // Compute transposed indices of X
        auto indices_range_x =
            std::views::iota(0, BoardSize) |
            std::views::transform([&](int32 x) { return ((BoardRange + x - players[id].pos[0]) % (BoardSize)) + (BoardRange + x >= players[id].pos[0] ? 0 : BoardSize); });
        // Compute normal perm indices of Y
        auto indices_range_y =
            std::views::iota(-BoardRange, BoardRange + 1) | std::views::transform([&](int32 y) { return ((players[id].pos[1] + y) % (BoardSize)) + (players[id].pos[1] + y >= 0 ? 0 : BoardSize); });

        auto pmat_x = Eigen::PermutationMatrix<BoardSize>(Eigen::Vector<int32, BoardSize>(std::vector<int>(indices_range_x.begin(), indices_range_x.end()).data()));
        auto pmat_y = Eigen::PermutationMatrix<BoardSize>(Eigen::Vector<int32, BoardSize>(std::vector<int>(indices_range_y.begin(), indices_range_y.end()).data()));

        return State<Players, HammerStrength, ViewRange, Scalar>{
            .view = (pmat_x * world * pmat_y)(Eigen::seq(BoardRange - ViewRange, BoardRange + ViewRange), Eigen::seq(BoardRange - ViewRange, BoardRange + ViewRange)),
        };
    }

    float64 GetScore(int32 id)
    {
        return colors_top[players[id].color_original] + colors[players[id].color];
    }

  public:
    Game(std::ranlux48_base *generator) : BaseGame<State<Players, HammerStrength, ViewRange, Scalar>, Move<Scalar>, Players>(generator)
    {
        std::tie(maze_template, spawnpoints) = GenerateMaze();
    }

    ~Game() {}
};

} // namespace Catch
