#pragma once
#include <algorithm>
#include <array>
#include <cmath>
#include <random>
#include <ranges>
#include "SFML/Graphics.hpp"
#include "base.hpp"
#include "concurrentqueue.h"
#include "Eigen/Dense"
#include "magic_enum.hpp"

namespace Curve
{
enum class Entity : int32
{
    NONE,
    WALL,
};

template <int32 ViewRange, typename Scalar>
struct State
{
    using ScalarT = Scalar;

    static constexpr int32 ViewSize = ViewRange * 2 + 1;
    static constexpr size_t Length = ViewSize * ViewSize + 2;
    static constexpr size_t RDomain = std::pow(ViewRange + 1, 4);

    Eigen::Matrix<Entity, ViewSize, ViewSize> view;
    Eigen::Vector2i direction;
    bool alive;

    sf::VertexArray *DrawData() const
    {
        auto vertices = new sf::VertexArray();
        vertices->setPrimitiveType(sf::Quads);
        const float32 scale = 1 / (2 * static_cast<float32>(ViewRange) + 1);

        for (int32 y = 0; y < view.cols(); y++)
            for (int32 x = 0; x < view.rows(); x++)
            {
                std::array<uint8, 3> color = {0, 0, 0};
                if (view(x, y) == Entity::NONE)
                    continue;

                switch (view(x, y))
                {
                case Entity::WALL:
                    color = {128, 128, 0};
                    break;
                }

                for (auto &&[dx, dy] : {std::array<float32, 2>{0, 0}, {0, 1}, {1, 1}, {1, 0}})
                {
                    vertices->append({{(x + dx) * scale, (y + dy) * scale}, {color[0], color[1], color[2]}});
                }
            }
        return vertices;
    }

    const Eigen::Vector<Scalar, Length> ToVec() const
    {
        auto vec = Eigen::Vector<Scalar, Length>();

        for (auto &&[e, i] : std::views::zip(view.reshaped(), std::views::iota(0)))
            vec[i] = static_cast<Scalar>(e);

        for (auto &&[e, i] : std::views::zip(direction, std::views::iota(ViewSize * ViewSize)))
            vec[i] = static_cast<Scalar>(e);

        return vec;
    }

    size_t RIndex() const
    {
        auto dir_1 = Eigen::Matrix2<int32>{{0, -1}, {1, 0}} * direction;
        auto dir_2 = Eigen::Matrix2<int32>{{0, -1}, {1, 0}} * dir_1;
        auto dir_3 = Eigen::Matrix2<int32>{{0, -1}, {1, 0}} * dir_2;

        size_t index = 0;
        for (const auto &[dir, j] : std::views::zip(std::array{direction, dir_1.eval(), dir_2.eval(), dir_3.eval()}, std::array{0, 1, 2, 3}))
        {
            int32 clear = 0;
            for (int32 i = 0; i < ViewRange; i++, clear++)
                if (view(ViewRange + (i + 1) * dir[0], ViewRange + (i + 1) * dir[1]) != Entity::NONE)
                    break;
            index += clear * std::pow(ViewRange + 1, j);
        }
        return index;
    }

    ScalarT RValue() const
    {
        ScalarT clear = 0;
        for (int32 i = 0; i < ViewRange; i++, clear++)
            if (view(ViewRange + (i + 1) * direction[0], ViewRange + (i + 1) * direction[1]) != Entity::NONE)
                break;
        return clear;
    }

    bool RDone() const
    {
        return not alive;
    }
};

enum class Action : uint8
{
    NONE,
    LEFT,
    RIGHT,
};

template <typename Scalar>
struct Move
{
    using ScalarT = Scalar;

    static constexpr size_t Length = magic_enum::enum_count<Action>();
    static constexpr size_t MDomain = magic_enum::enum_count<Action>();

    Action action;

    Move() : action(Action::NONE) {}

    Move(const Action &action) : action(action) {}

    Move(moodycamel::ConcurrentQueue<sf::Keyboard::Key> &keys)
    {
        sf::Keyboard::Key buff[10];
        size_t len = keys.try_dequeue_bulk(buff, 10);
        if (not len)
        {
            action = Action::NONE;
            return;
        }

        auto key = buff[len - 1];
        if (key == sf::Keyboard::A)
            action = Action::LEFT;
        else if (key == sf::Keyboard::D)
            action = Action::RIGHT;
        else
            action = Action::NONE;
    }

    Move(std::ranlux48_base *generator)
    {
        action = static_cast<Action>(std::uniform_int_distribution<>(0, magic_enum::enum_count<Action>() - 1)(*generator));
    }

    Move(const Eigen::Vector<Scalar, Length> &vector)
    {
        action = static_cast<Action>(std::distance(vector.begin(), std::max_element(vector.begin(), vector.end())));
    }

    Move(const size_t &n)
    {
        action = static_cast<Action>(n);
    }

    template <typename T>
    static Move Heuristic(std::ranlux48_base *generator, const T &state)
    {
        static constexpr int32 Center = (T::ViewSize - 1) / 2;
        auto left_turn = Eigen::Matrix2<int32>{{0, 1}, {-1, 0}} * state.direction;

        if (state.view(Center + state.direction[0], Center + state.direction[1]) == Entity::WALL and state.view(Center + left_turn[0], Center + left_turn[1]) != Entity::WALL)
            return Move<Scalar>(Action::LEFT);
        if (state.view(Center + state.direction[0], Center + state.direction[1]) == Entity::WALL)
            return Move<Scalar>(Action::RIGHT);

        return Move<Scalar>(Action::NONE);
    }
};

template <int32 Players, int32 BoardRange = 32, int32 ViewRange = 6, typename Scalar = float32>
requires requires { BoardRange >= ViewRange; }
class Game : public BaseGame<State<ViewRange, Scalar>, Move<Scalar>, Players>
{
    static constexpr int32 BoardSize = BoardRange * 2 + 1;

    struct Player
    {
        Eigen::Vector2<int32> pos;
        Eigen::Vector2<int32> vel;
        bool alive;
        int32 lifespan;
    };

    Eigen::Matrix<Entity, BoardSize, BoardSize> world;
    std::array<Player, Players> players;
    int32 deaths;

    bool Tick(const std::array<Move<Scalar>, Players> &moves)
    {
        for (auto &&[player, move] : std::views::zip(players, moves))
        {
            if (not player.alive)
                continue;

            switch (move.action)
            {
            case Action::LEFT:
                player.vel = Eigen::Matrix2<int32>{{0, 1}, {-1, 0}} * player.vel;
                break;

            case Action::RIGHT:
                player.vel = Eigen::Matrix2<int32>{{0, -1}, {1, 0}} * player.vel;
                break;
            }

            player.pos += player.vel;
            player.pos = player.pos.unaryExpr([this](const int32 x) { return x == BoardSize ? 0 : x == -1 ? BoardRange * 2 : x; });

            if (world(player.pos[0], player.pos[1]) == Entity::WALL)
            {
                player.alive = false;
                deaths++;

                for (auto &p : players | std::views::filter([](auto p) { return p.alive; }))
                    p.lifespan++;
            }
        }

        for (auto &&player : players)
            world(player.pos[0], player.pos[1]) = Entity::WALL;

        return deaths >= players.size() - 1 ? true : false;
    }

    void Restart()
    {
        for (auto &&e : world.reshaped())
            e = Entity::NONE;

        std::array<uint8, BoardSize> pos_x, pos_y;
        std::iota(pos_x.begin(), pos_x.end(), 0);
        std::iota(pos_y.begin(), pos_y.end(), 0);
        std::shuffle(pos_x.begin(), pos_x.end(), *this->generator);
        std::shuffle(pos_y.begin(), pos_y.end(), *this->generator);

        for (auto &&[player, x, y] : std::views::zip(players, pos_x, pos_y))
        {
            std::array<Eigen::Vector2<int32>, 4> directions = {{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};
            std::shuffle(directions.begin(), directions.end(), *this->generator);

            player = Player{
                .pos = {x, y},
                .vel = directions[0],
                .alive = true,
                .lifespan = 0,
            };
            world(x, y) = Entity::WALL;
        }

        deaths = 0;
    }

    State<ViewRange, Scalar> GetState(int32 id)
    {
        // Compute transposed indices of X
        auto indices_range_x =
            std::views::iota(0, BoardSize) |
            std::views::transform([&](int32 x) { return ((BoardRange + x - players[id].pos[0]) % (BoardSize)) + (BoardRange + x >= players[id].pos[0] ? 0 : BoardSize); });
        // Compute normal perm indices of Y
        auto indices_range_y =
            std::views::iota(-BoardRange, BoardRange + 1) | std::views::transform([&](int32 y) { return ((players[id].pos[1] + y) % (BoardSize)) + (players[id].pos[1] + y >= 0 ? 0 : BoardSize); });

        auto pmat_x = Eigen::PermutationMatrix<BoardSize>(Eigen::Vector<int32, BoardSize>(std::vector<int32>(indices_range_x.begin(), indices_range_x.end()).data()));
        auto pmat_y = Eigen::PermutationMatrix<BoardSize>(Eigen::Vector<int32, BoardSize>(std::vector<int32>(indices_range_y.begin(), indices_range_y.end()).data()));

        return State<ViewRange, Scalar>{
            .view = (pmat_x * world * pmat_y)(Eigen::seq(BoardRange - ViewRange, BoardRange + ViewRange), Eigen::seq(BoardRange - ViewRange, BoardRange + ViewRange)),
            .direction = players[id].vel,
            .alive = players[id].alive,
        };
    }

    float64 GetScore(int32 id)
    {
        return static_cast<float64>(players[id].lifespan);
    }

  public:
    Game(std::ranlux48_base *generator) : BaseGame<State<ViewRange, Scalar>, Move<Scalar>, Players>(generator) {}

    ~Game() {}
};
} // namespace Curve
