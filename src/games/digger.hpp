#pragma once
#include <array>
#include <cmath>
#include <random>
#include <ranges>
#include <vector>
#include "Eigen/Dense"
#include "SFML/Graphics.hpp"
#include "base.hpp"
#include "concurrentqueue.h"
#include "magic_enum.hpp"

namespace Digger
{
struct Player
{
    Eigen::Vector2<int32> pos;
    Eigen::Vector2<int32> pos_d;
    int32 metal;
    int32 oxygen;
    int32 crystals;
    int32 timeout;
    int32 lifespan;
    bool alive;
};

struct Trap
{
    Player *owner;
    Eigen::Vector2<int32> pos;
};

struct Rocket
{
    Player *owner;
    Eigen::Vector2<int32> pos;
    Eigen::Vector2<int32> direction;
    int32 lifespan;
};

struct Entity
{
    enum class Type : uint8
    {
        NONE,
        WALL,
        OXYLITE,
        CRYSTAL,
        TRAP,
        ROCKET,
        PLAYER,
        UNKNOWN,
    } type;
    void *object;
};

template <int32 ViewRange, typename Scalar>
struct State
{
    static constexpr int32 ViewSize = ViewRange * 2 + 1;
    Eigen::Matrix<Entity, ViewSize, ViewSize> view;

    sf::VertexArray *DrawData() const
    {
        auto vertices = new sf::VertexArray();
        vertices->setPrimitiveType(sf::Quads);
        const float32 scale = 1 / (2 * static_cast<float32>(ViewRange) + 1);

        for (int32 y = 0; y < view.cols(); y++)
            for (int32 x = 0; x < view.rows(); x++)
            {
                std::array<uint8, 3> color = {0, 0, 0};
                switch (view(x, y).type)
                {
                case Entity::Type::NONE:
                    continue;

                case Entity::Type::WALL:
                    color = {128, 128, 0};
                    break;

                case Entity::Type::OXYLITE:
                    color = {0, 128, 128};
                    break;

                case Entity::Type::CRYSTAL:
                    color = {255, 255, 255};
                    break;

                case Entity::Type::TRAP:
                    color = {255, 0, 0};
                    break;

                case Entity::Type::ROCKET:
                    color = {255, 128, 0};
                    break;

                case Entity::Type::PLAYER:
                    color = {0, 255, 0};
                    break;

                case Entity::Type::UNKNOWN:
                    color = {128, 128, 128};
                    break;
                }

                for (auto &&[dx, dy] : {std::array<float32, 2>{0, 0}, {0, 1}, {1, 1}, {1, 0}})
                {
                    vertices->append({{(x + dx) * scale, (y + dy) * scale}, {color[0], color[1], color[2]}});
                }
            }
        return vertices;
    }

    using ScalarT = Scalar;
    const static size_t Length = ViewSize * ViewSize * (magic_enum::enum_count<Entity::Type>() - 1) + 2;

    const Eigen::Vector<Scalar, Length> ToVec() const
    {
        auto vec = Eigen::Vector<Scalar, Length>();
        for (auto &v : vec)
            v = 0;

        int32 i = 0;
        for (auto &e : view.reshaped())
        {
            if (e.type == Entity::Type::NONE)
            {
                i += magic_enum::enum_count<Entity::Type>() - 1;
                continue;
            }
            vec[i + (int32)e.type - 1] = 1;
            i += magic_enum::enum_count<Entity::Type>() - 1;
        }
        const auto &player = static_cast<Player *>(view(ViewRange, ViewRange).object);
        vec[i++] = player->pos_d[0];
        vec[i] = player->pos_d[1];

        return vec;
    }
};

enum class MAction : uint8
{
    NONE,
    MOVE_UP,
    MOVE_DOWN,
    MOVE_LEFT,
    MOVE_RIGHT,
};

enum class HAction : uint8
{
    NONE,
    BUILD,
    PLACE_TRAP,
    FIRE_ROCKET,
};

template <typename Scalar>
struct Move
{
    using ScalarT = Scalar;

    static constexpr size_t Length = magic_enum::enum_count<MAction>() + magic_enum::enum_count<HAction>();
    static constexpr size_t MDomain = magic_enum::enum_count<MAction>() * magic_enum::enum_count<HAction>();

    MAction m_action;
    HAction h_action;

    Move()
    {
        m_action = MAction::NONE;
        h_action = HAction::NONE;
    }

    Move(const MAction &m_action, const HAction &h_action) : m_action(m_action), h_action(h_action) {}

    Move(moodycamel::ConcurrentQueue<sf::Keyboard::Key> &keys)
    {
        sf::Keyboard::Key buff[10];
        size_t len = keys.try_dequeue_bulk(buff, 10);

        m_action = MAction::NONE;
        h_action = HAction::NONE;

        for (int32 i = 0; i < len; i++)
            switch (buff[i])
            {
            case sf::Keyboard::W:
                m_action = MAction::MOVE_UP;
                break;

            case sf::Keyboard::S:
                m_action = MAction::MOVE_DOWN;
                break;

            case sf::Keyboard::A:
                m_action = MAction::MOVE_LEFT;
                break;

            case sf::Keyboard::D:
                m_action = MAction::MOVE_RIGHT;
                break;

            case sf::Keyboard::Space:
                h_action = HAction::BUILD;
                break;

            case sf::Keyboard::Q:
                h_action = HAction::FIRE_ROCKET;
                break;

            case sf::Keyboard::E:
                h_action = HAction::PLACE_TRAP;
                break;
            }
    }

    Move(std::ranlux48_base *generator)
    {
        m_action = static_cast<MAction>(std::uniform_int_distribution<>(0, magic_enum::enum_count<MAction>() - 1)(*generator));
        h_action = static_cast<HAction>(std::uniform_int_distribution<>(0, magic_enum::enum_count<HAction>() - 1)(*generator));
    }

    Move(const Eigen::Vector<Scalar, Length> &vector)
    {
        m_action = static_cast<MAction>(std::distance(vector.begin(), std::max_element(vector.begin(), vector.begin() + magic_enum::enum_count<MAction>())));
        h_action = static_cast<HAction>(std::distance(vector.begin() + magic_enum::enum_count<MAction>(), std::max_element(vector.begin() + magic_enum::enum_count<MAction>(), vector.end())));
    }

    Move(const size_t &n)
    {
        m_action = static_cast<MAction>(n % magic_enum::enum_count<MAction>());
        h_action = static_cast<HAction>((n - size_t(m_action)) / magic_enum::enum_count<MAction>());
    }

    template <typename T>
    static Move Heuristic(std::ranlux48_base *generator, const T &state)
    {
        static constexpr int32 Center = (T::ViewSize - 1) / 2;
        std::tuple<int32, int32, int32, Entity::Type> target = {0, 0, INT32_MAX, Entity::Type::NONE};
        auto m_action = static_cast<MAction>(std::uniform_int_distribution<>(0, magic_enum::enum_count<MAction>() - 1)(*generator));
        auto h_action = HAction::PLACE_TRAP;

        auto dist = [](int32 x, int32 y)
        {
            static constexpr int32 Center = (T::ViewSize - 1) / 2;
            return std::abs(Center - x) + std::abs(Center - y);
        };

        auto action = [](int32 x, int32 y)
        {
            static constexpr int32 Center = (T::ViewSize - 1) / 2;
            return y < Center ? MAction::MOVE_UP : y > Center ? MAction::MOVE_DOWN : x < Center ? MAction::MOVE_LEFT : MAction::MOVE_RIGHT;
        };

        for (int32 y = 0; y < state.view.cols(); y++)
            for (int32 x = 0; x < state.view.rows(); x++)
                if ((state.view(x, y).type == Entity::Type::WALL or state.view(x, y).type == Entity::Type::OXYLITE or state.view(x, y).type == Entity::Type::CRYSTAL) and
                    (dist(x, y) < std::get<2>(target) or size_t(state.view(x, y).type) > size_t(std::get<3>(target))))
                {
                    target = {x, y, dist(x, y), state.view(x, y).type};
                    m_action = action(x, y);
                }

        auto &&m_vector = std::map<MAction, std::array<int32, 2>>{{MAction::MOVE_UP, {0, -1}}, {MAction::MOVE_DOWN, {0, 1}}, {MAction::MOVE_LEFT, {-1, 0}}, {MAction::MOVE_RIGHT, {1, 0}}}[m_action];

        if (state.view(Center + m_vector[0], Center + m_vector[1]).type == Entity::Type::TRAP or state.view(Center + m_vector[0], Center + m_vector[1]).type == Entity::Type::ROCKET or
            state.view(Center + m_vector[0], Center + m_vector[1]).type == Entity::Type::PLAYER)
            return Move(static_cast<MAction>(std::uniform_int_distribution<>(0, magic_enum::enum_count<MAction>() - 1)(*generator)), HAction::NONE);

        return Move(m_action, h_action);
    }
};

template <
    int32 Players,
    int32 BoardRange = 32,
    int32 ViewRange = 6,
    typename Scalar = float32,
    int32 InitialMetal = 0,
    int32 InitialOxygen = 30,
    int32 InitialCrystals = 1,
    int32 OxyliteGain = 10,
    int32 DigHardness = 3,
    std::pair<int32, int32> BuildCost = {2, 0},
    std::pair<int32, int32> TrapCost = {25, 2},
    std::pair<int32, int32> RocketCost = {15, 1},
    int32 RocketLifespan = 10,
    int32 RocketVelocity = 2,
    float64 OxyliteDensity = 0.05f64,
    float64 OxyliteVeinSize = 0.5f64,
    float64 CrystalDensity = 0.01f64>
requires requires { BoardRange >= ViewRange; }
class Game : public BaseGame<State<ViewRange, Scalar>, Move<Scalar>, Players>
{
    static constexpr int32 BoardSize = BoardRange * 2 + 1;
    using Board = Eigen::Matrix<Entity, BoardSize, BoardSize>;
    using Coord = std::array<int32, 2>;

    Board mine_template, world;
    std::array<Player, Players> players;
    std::vector<Trap> traps;
    std::vector<Rocket> rockets;

    int32 alive;

    Board GenerateMine()
    {
        Board blank;
        for (auto &&e : blank.reshaped())
            e = Entity{
                .type = Entity::Type::WALL,
            };

        auto perm_x = std::array<int32, BoardSize>();
        auto perm_y = std::array<int32, BoardSize>();
        std::ranges::iota(perm_x, 0);
        std::ranges::iota(perm_y, 0);
        std::ranges::shuffle(perm_x, *this->generator);
        std::ranges::shuffle(perm_y, *this->generator);

        auto generators = std::array<void (*)(Board &, std::ranlux48_base *, const int32 &, const int32 &), 2>{
            [](Board &board, std::ranlux48_base *generator, const int32 &x, const int32 &y)
            {
                if (std::discrete_distribution<>{1 - OxyliteDensity, OxyliteDensity}(*generator))
                {
                    auto size = std::array{1, 1};
                    while (std::discrete_distribution<>{1 - OxyliteVeinSize, OxyliteVeinSize}(*generator) and size[0] <= BoardRange)
                        size[0]++;
                    while (std::discrete_distribution<>{1 - OxyliteVeinSize, OxyliteVeinSize}(*generator) and size[0] <= BoardRange)
                        size[1]++;

                    for (auto [dx, dy] : std::views::cartesian_product(std::views::iota(0, size[0]), std::views::iota(0, size[1])))
                        board((x + dx) % BoardSize, (y + dy) % BoardSize) = Entity{
                            .type = Entity::Type::OXYLITE,
                        };
                }
            },
            [](Board &board, std::ranlux48_base *generator, const int32 &x, const int32 &y)
            {
                if (std::discrete_distribution<>{1 - CrystalDensity, CrystalDensity}(*generator))
                {
                    board(x, y) = Entity{
                        .type = Entity::Type::CRYSTAL,
                    };
                }
            }};

        for (auto &&[y, x] : std::views::cartesian_product(perm_y, perm_x)) // for efficiency purposes
        {
            std::ranges::shuffle(generators, *this->generator);
            for (auto generator : generators)
                generator(blank, this->generator, x, y);
        }

        return blank;
    }

    bool Tick(const std::array<Move<Scalar>, Players> &moves)
    {
        auto indices = std::array<int32, Players>();
        std::iota(indices.begin(), indices.end(), 0);
        std::ranges::shuffle(indices, *this->generator);

        auto mod = [](int32 i) constexpr { return (i % BoardSize + BoardSize) % BoardSize; };

        for (auto &index : indices)
        {
            auto &&[player, move] = std::tie(players[index], moves[index]);
            auto &m_action = move.m_action;

            if (m_action == MAction::NONE or player.timeout or not player.alive)
                continue;

            auto &&[x, y] = std::tie(player.pos[0], player.pos[1]);
            auto &&[dx, dy] = std::tie(player.pos_d[0], player.pos_d[1]);
            auto &&[tx, ty] = std::map<MAction, Coord>{{MAction::MOVE_UP, {0, -1}}, {MAction::MOVE_DOWN, {0, 1}}, {MAction::MOVE_LEFT, {-1, 0}}, {MAction::MOVE_RIGHT, {1, 0}}}[m_action];
            auto &&[nx, ny] = std::tuple{mod(x + tx), mod(y + ty)};

            switch (world(nx, ny).type)
            {
            case Entity::Type::NONE:
            {
                world(nx, ny) = world(x, y);
                world(x, y).type = Entity::Type::NONE;
                std::tie(dx, dy) = std::tuple{tx, ty};
                std::tie(x, y) = std::tuple{nx, ny};
            }
            break;
            case Entity::Type::WALL:
            {
                world(nx, ny) = world(x, y);
                world(x, y).type = Entity::Type::NONE;
                std::tie(dx, dy) = std::tuple{tx, ty};
                std::tie(x, y) = std::tuple{nx, ny};

                player.metal++;
                player.timeout += DigHardness + 1;
            }
            break;
            case Entity::Type::OXYLITE:
            {
                world(nx, ny) = world(x, y);
                world(x, y).type = Entity::Type::NONE;
                std::tie(dx, dy) = std::tuple{tx, ty};
                std::tie(x, y) = std::tuple{nx, ny};

                player.oxygen += OxyliteGain;
                player.timeout += DigHardness + 1;
            }
            break;
            case Entity::Type::CRYSTAL:
            {
                world(nx, ny) = world(x, y);
                world(x, y).type = Entity::Type::NONE;
                std::tie(dx, dy) = std::tuple{tx, ty};
                std::tie(x, y) = std::tuple{nx, ny};

                player.crystals++;
                player.timeout += DigHardness + 1;
            }
            break;
            }
        }

        for (auto &index : indices)
        {
            auto &&[player, move] = std::tie(players[index], moves[index]);
            auto &h_action = move.h_action;

            if (h_action == HAction::NONE or not player.alive)
                continue;

            auto &&[x, y] = std::tie(player.pos[0], player.pos[1]);
            auto &&[dx, dy] = std::tie(player.pos_d[0], player.pos_d[1]);
            auto &&[nx, ny] = std::tuple{mod(x + dx), mod(y + dy)};
            auto &&[metal, crystals] = std::tie(player.metal, player.crystals);

            if (!dx and !dy)
                continue;

            switch (h_action)
            {
            case HAction::BUILD:
            {
                if (metal >= BuildCost.first and crystals >= BuildCost.second and world(nx, ny).type == Entity::Type::NONE)
                {
                    world(nx, ny).type = Entity::Type::WALL;
                    metal -= BuildCost.first;
                    crystals -= BuildCost.second;
                }
            }
            break;
            case HAction::PLACE_TRAP:
            {
                if (metal >= TrapCost.first and crystals >= TrapCost.second and world(nx, ny).type == Entity::Type::NONE)
                {
                    traps.push_back(Trap{
                        .owner = &player,
                        .pos = {nx, ny},
                    });
                    world(nx, ny) = Entity{
                        .type = Entity::Type::TRAP,
                        .object = &traps.back(),
                    };
                    metal -= TrapCost.first;
                    crystals -= TrapCost.second;
                }
            }
            break;
            case HAction::FIRE_ROCKET:
            {
                if (metal >= RocketCost.first and crystals >= RocketCost.second and world(nx, ny).type == Entity::Type::NONE)
                {
                    rockets.push_back(Rocket{
                        .owner = &player,
                        .pos = {nx, ny},
                        .direction = {dx, dy},
                        .lifespan = RocketLifespan,
                    });
                    world(nx, ny) = Entity{
                        .type = Entity::Type::ROCKET,
                        .object = &rockets.back(),
                    };
                    metal -= RocketCost.first;
                    crystals -= RocketCost.second;
                }
            }
            break;
            }
        }

        for (auto &index : indices)
        {
            if (not players[index].alive)
                continue;
            if (players[index].timeout)
                players[index].timeout--;
            if (not --players[index].oxygen) // Suffocation
            {
                players[index].alive = false;
                alive--;
            }
        }

        for (auto iter = traps.begin(); iter != traps.end();)
        {
            auto &trap = *iter;
            const auto &[x, y] = std::tie(trap.pos[0], trap.pos[1]);
            bool exploded = false;

            for (const auto &p :
                 std::views::cartesian_product(std::views::iota(-2, 3), std::views::iota(-2, 3)) |
                     std::views::filter(
                         [](auto p)
                         {
                             auto [tx, ty] = p;
                             return tx != 0 or ty != 0;
                         }))
            {
                const auto &[dx, dy] = p;
                auto [nx, ny] = std::tuple{mod(x + dx), mod(y + dy)};
                auto player = static_cast<Player *>(world(nx, ny).object);
                if (world(nx, ny).type == Entity::Type::PLAYER and player != trap.owner)
                {
                    exploded = true;
                    player->alive = false;
                    alive--; // Death to a trap
                    trap.owner->metal += player->metal;
                    trap.owner->oxygen += player->oxygen;
                    trap.owner->crystals += player->crystals;

                    world(nx, ny).type = Entity::Type::NONE;
                }
            }

            if (exploded)
            {
                world(x, y).type = Entity::Type::NONE;
                iter = traps.erase(iter);
                continue;
            }

            iter++;
        }

        for (auto iter = rockets.begin(); iter != rockets.end();)
        {
            auto &rocket = *iter;
            bool exploded = false;

            for (auto i : std::views::iota(0, RocketVelocity))
            {
                auto &&[x, y] = std::tie(rocket.pos[0], rocket.pos[1]);
                auto &&[dx, dy] = std::tie(rocket.direction[0], rocket.direction[1]);
                auto [nx, ny] = std::tuple{mod(x + dx), mod(y + dy)};
                auto player = static_cast<Player *>(world(nx, ny).object);

                if (world(nx, ny).type != Entity::Type::NONE and world(nx, ny).type != Entity::Type::PLAYER)
                {
                    exploded = true;
                    break;
                }
                if (world(nx, ny).type == Entity::Type::PLAYER)
                {
                    exploded = true;
                    player->alive = false;
                    alive--; // Blown up by a rocket
                    rocket.owner->metal += player->metal;
                    rocket.owner->oxygen += player->oxygen;
                    rocket.owner->crystals += player->crystals;

                    world(nx, ny).type = Entity::Type::NONE;
                    break;
                }
                if (not --rocket.lifespan)
                {
                    exploded = true;
                    break;
                }
                if (world(nx, ny).type == Entity::Type::NONE)
                {
                    world(x, y).type = Entity::Type::NONE;
                    world(nx, ny) = Entity{
                        .type = Entity::Type::ROCKET,
                        .object = &rocket,
                    };
                    x = nx;
                    y = ny;
                }
            }

            if (exploded)
            {
                auto &&[x, y] = std::tie(rocket.pos[0], rocket.pos[1]);
                world(x, y).type = Entity::Type::NONE;
                iter = rockets.erase(iter);
                continue;
            }

            iter++;
        }
        for (auto &p : players | std::views::filter([](auto p) { return p.alive; }))
            p.lifespan++;

        return alive <= 1;
    };

    void Restart()
    {
        world = mine_template;
        traps.clear();
        rockets.clear();
        alive = Players;

        std::array<uint8, BoardSize> pos_x, pos_y;
        std::iota(pos_x.begin(), pos_x.end(), 0);
        std::iota(pos_y.begin(), pos_y.end(), 0);
        std::shuffle(pos_x.begin(), pos_x.end(), *this->generator);
        std::shuffle(pos_y.begin(), pos_y.end(), *this->generator);

        for (auto &&[player, x, y] : std::views::zip(players, pos_x, pos_y))
        {
            player = Player{
                .pos = {x, y},
                .pos_d = {0, 0},
                .metal = InitialMetal,
                .oxygen = InitialOxygen,
                .crystals = InitialCrystals,
                .timeout = 0,
                .lifespan = 0,
                .alive = true,
            };
            world(x, y) = Entity{
                .type = Entity::Type::PLAYER,
                .object = &player,
            };
        }
    };

    State<ViewRange, Scalar> GetState(int32 id)
    {
        // Compute transposed indices of X
        auto indices_range_x =
            std::views::iota(0, BoardSize) |
            std::views::transform([&](int32 x) { return ((BoardRange + x - players[id].pos[0]) % (BoardSize)) + (BoardRange + x >= players[id].pos[0] ? 0 : BoardSize); });
        // Compute normal perm indices of Y
        auto indices_range_y =
            std::views::iota(-BoardRange, BoardRange + 1) | std::views::transform([&](int32 y) { return ((players[id].pos[1] + y) % (BoardSize)) + (players[id].pos[1] + y >= 0 ? 0 : BoardSize); });

        auto pmat_x = Eigen::PermutationMatrix<BoardSize>(Eigen::Vector<int32, BoardSize>(std::vector<int>(indices_range_x.begin(), indices_range_x.end()).data()));
        auto pmat_y = Eigen::PermutationMatrix<BoardSize>(Eigen::Vector<int32, BoardSize>(std::vector<int>(indices_range_y.begin(), indices_range_y.end()).data()));
        auto view = (pmat_x * world * pmat_y)(Eigen::seq(BoardRange - ViewRange, BoardRange + ViewRange), Eigen::seq(BoardRange - ViewRange, BoardRange + ViewRange)).eval();

        for (const auto [sy, sx] : {std::array<int32, 2>{0, 0}, {0, ViewRange * 2}, {ViewRange * 2, 0}, {ViewRange * 2, ViewRange * 2}})
        {
            int32 dy = std::signbit(ViewRange - sy) ? -1 : 1;
            int32 dx = std::signbit(ViewRange - sx) ? -1 : 1;
            for (int32 y = sy; y != ViewRange; y += dy)
                for (int32 x = sx; x != ViewRange; x += dx)
                {
                    bool hit = 0;
                    for (int32 py = y; py != ViewRange; py += dy)
                    {
                        for (int32 px = x; px != ViewRange; px += dx)
                        {
                            if (px == x and py == y)
                                continue;

                            const auto &[x0, y0, x1, y1, x2, y2] = std::tuple{px, py, x, y, ViewRange, ViewRange};
                            float64 distance = std::abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1)) / std::sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
                            if (distance <= std::sqrt(2) and view(x0, y0).type != Entity::Type::NONE and view(x0, y0).type != Entity::Type::TRAP)
                                hit = true;
                            if (hit)
                                break;
                        }
                        if (hit)
                            break;
                    }
                    if (hit)
                        view(x, y).type = Entity::Type::UNKNOWN;
                }
        }
        for (int32 x = ViewRange - 1, y = ViewRange, hit = 0; x > -1; x--)
        {
            if (hit)
            {
                view(x, y).type = Entity::Type::UNKNOWN;
                continue;
            }
            if (view(x, y).type != Entity::Type::NONE and view(x, y).type != Entity::Type::TRAP)
                hit = 1;
        }
        for (int32 x = ViewRange + 1, y = ViewRange, hit = 0; x < ViewRange * 2 + 1; x++)
        {
            if (hit)
            {
                view(x, y).type = Entity::Type::UNKNOWN;
                continue;
            }
            if (view(x, y).type != Entity::Type::NONE and view(x, y).type != Entity::Type::TRAP)
                hit = 1;
        }
        for (int32 x = ViewRange, y = ViewRange - 1, hit = 0; y > -1; y--)
        {
            if (hit)
            {
                view(x, y).type = Entity::Type::UNKNOWN;
                continue;
            }
            if (view(x, y).type != Entity::Type::NONE and view(x, y).type != Entity::Type::TRAP)
                hit = 1;
        }
        for (int32 x = ViewRange, y = ViewRange + 1, hit = 0; y < ViewRange * 2 + 1; y++)
        {
            if (hit)
            {
                view(x, y).type = Entity::Type::UNKNOWN;
                continue;
            }
            if (view(x, y).type != Entity::Type::NONE and view(x, y).type != Entity::Type::TRAP)
                hit = 1;
        }
        for (auto &&e : view.reshaped())
            if (e.type == Entity::Type::TRAP and static_cast<Rocket *>(e.object)->owner != &players[id])
                e.type = Entity::Type::NONE;

        return State<ViewRange, Scalar>{
            .view = view,
        };
    };

    float64 GetScore(int32 id)
    {
        return static_cast<float64>(players[id].lifespan);
    };

  public:
    Game(std::ranlux48_base *generator) : BaseGame<State<ViewRange, Scalar>, Move<Scalar>, Players>(generator)
    {
        mine_template = GenerateMine();
    };

    ~Game() {}
};

} // namespace Digger
