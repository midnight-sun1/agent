#pragma once
#include <array>
#include <atomic>
#include <chrono>
#include <concepts>
#include <mutex>
#include <numeric>
#include <random>
#include <thread>
#include <typeinfo>
#include "SFML/Graphics.hpp"
#include "concurrentqueue.h"
#include "config.hpp"
#include "Eigen/Dense"
#include "serializer.hpp"

// clang-format off

template <typename T>
concept IToDraw = requires(const T &x)
{
    { x.DrawData() } -> std::same_as<sf::VertexArray *>;
};

template <typename T>
concept IFromKeyboard = requires(moodycamel::ConcurrentQueue<sf::Keyboard::Key> &keys)
{
    T(keys);
};

template <typename T>
concept IFromGenerator = requires(std::ranlux48_base *generator)
{
    T(generator);
};

template <typename T>
concept IToVec = requires(const T &state)
{
    { state.ToVec() } -> std::same_as<const Eigen::Vector<typename T::ScalarT, T::Length>>;
};

template <typename T>
concept IFromVec = requires(const Eigen::Vector<typename T::ScalarT, T::Length> &vec)
{
    T(vec);
};

template <typename T>
concept IToReinforced = requires(const T &state)
{
    { T::RDomain } -> std::same_as<const size_t&>;
    { state.RIndex() } -> std::same_as<size_t>;
    { state.RValue() } -> std::same_as<typename T::ScalarT>;
    { state.RDone() } -> std::same_as<bool>;
};

template <typename T>
concept IFromReinforced = requires
{
    T(T::MDomain);
    { T::MDomain } -> std::same_as<const size_t&>;
};

template <typename T>
concept IEvolution = requires(const T *x, const T *y, T *z, const float64 &strength)
{
    { T(x) };
    { z->Mutate(strength) } -> std::same_as<void>;
    { x->Cross(y) } -> std::same_as<T *>;
    { x->Save() };
};

// clang-format on

template <IToDraw State, typename Move>
class BaseAgent
{
    template <typename, typename, int32>
    friend class BaseGame;

    std::vector<float64> scores;
    std::thread *draw_thread;
    std::mutex draw_mutex;
    std::atomic<bool> end;

    sf::VertexArray *vertices;
    sf::RenderWindow *window;

    void End(const float64 &score)
    {
        scores.push_back(score);
    }

    void DrawInit()
    {
        end = false;
        vertices = new sf::VertexArray();
        draw_thread = new std::thread(
            [this]()
            {
                auto transform = sf::Transform();
                transform.scale({WindowSize, WindowSize});

                window = new sf::RenderWindow(sf::VideoMode({WindowSize, WindowSize}), typeid(*this).name());
                window->setVerticalSyncEnabled(true);

                while (not end)
                {
                    sf::Event event;
                    while (window->pollEvent(event))
                    {
                        if (event.type == sf::Event::KeyPressed)
                            keys.enqueue(event.key.code);
                    }

                    window->clear();
                    draw_mutex.lock();
                    window->draw(*vertices, sf::RenderStates(transform));
                    draw_mutex.unlock();
                    window->display();
                }
                window->close();
                delete window;
            });
    }

    Move DrawProcess(const State &state)
    {
        draw_mutex.lock();
        delete vertices;
        vertices = state.DrawData();
        draw_mutex.unlock();

        return this->Process(state);
    }

    void DrawEnd()
    {
        end = true;
        draw_thread->join();
        delete draw_thread;
    }

  protected:
    std::ranlux48_base *generator;
    moodycamel::ConcurrentQueue<sf::Keyboard::Key> keys;

    virtual Move Process(const State &state) = 0;
    virtual void Reset() = 0;

  public:
    using StateT = State;
    using MoveT = Move;

    BaseAgent(std::ranlux48_base *generator) : generator(generator) {}

    virtual ~BaseAgent() {}

    float64 FlushScore()
    {
        auto score = std::accumulate(scores.begin(), scores.end(), (float64)0, [this](auto &&acc, auto &&ref) { return acc + ref; }) / scores.size();
        scores.clear();
        return score;
    }
};

template <typename State, typename Move, int32 Players>
class BaseGame
{
    std::array<BaseAgent<State, Move> *, Players> agents;
    std::array<Move, Players> moves;

  protected:
    virtual bool Tick(const std::array<Move, Players> &moves) = 0;
    virtual void Restart() = 0;
    virtual State GetState(int32 id) = 0;
    virtual float64 GetScore(int32 id) = 0;

    std::ranlux48_base *generator;

  public:
    using StateT = State;
    using MoveT = Move;
    static constexpr int32 PlayersV = Players;

    BaseGame(std::ranlux48_base *generator) : generator(generator) {}

    virtual ~BaseGame() {}

    void Play(const std::array<BaseAgent<State, Move> *, Players> &n_agents, int32 repeats)
    {
        agents = n_agents;
        if constexpr (Draw)
            for (auto agent : agents)
                agent->DrawInit();

        for (int32 i = 0; i < repeats; i++)
        {
            this->Restart();
            for (int32 i = 0; i < Players; i++)
                agents[i]->Reset();
            auto time = std::chrono::high_resolution_clock::now();

            while (true)
            {
                if constexpr (Draw)
                {
                    while (std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - time).count() < 1'000'000'000 / Tickrate) {}
                    time = std::chrono::high_resolution_clock::now();
                }
                for (int i = 0; i < Players; i++)
                {
                    if constexpr (not Draw)
                        moves[i] = agents[i]->Process(this->GetState(i));
                    if constexpr (Draw)
                        moves[i] = agents[i]->DrawProcess(this->GetState(i));
                }
                if (this->Tick(moves))
                    break;
            }
            for (int32 i = 0; i < Players; i++)
                agents[i]->End(this->GetScore(i));
        }
        if constexpr (Draw)
            for (auto agent : agents)
                agent->DrawEnd();
    }
};
