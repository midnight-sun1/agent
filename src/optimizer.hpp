#pragma once
#include <algorithm>
#include <format>
#include <initializer_list>
#include <iostream>
#include <queue>
#include <random>
#include <ranges>
#include <set>
#include <thread>
#include <type_traits>
#include "agents/dummy.hpp"
#include "base.hpp"

template <
    typename Game,
    typename Agent,
    int32 Islands = 1,
    int32 Population = 64,
    int32 EvalGameRep = 4,
    int32 EvalTournamentRep = 4,
    int32 EvalHofSize = 8,
    float64 EvalHofInfluence = 1.0f64,
    int32 CrossElitismSize = 4,
    int32 CrossTournamentSize = 4,
    float64 MutateProbability = 0.1f64,
    float64 MutateStrength = 0.1f64,
    typename State = typename Game::StateT,
    typename Move = typename Game::MoveT>
requires std::is_base_of_v<BaseGame<State, Move, Game::PlayersV>, Game> and IEvolution<Agent> and requires {
    Islands > 0;
    Population > 0;
    EvalGameRep > 0;
    EvalTournamentRep > 0;
    EvalHofSize >= 0;
    CrossElitismSize >= 0;
    CrossTournamentSize > 0;
    Population % Game::PlayersV == 0;
}
class Optimizer
{
    using PopulationT = std::array<std::tuple<Agent *, std::set<int32>, float64>, Population>;

    static constexpr auto Players = Game::PlayersV;
    static constexpr auto Matches = int32(Population / Players);

    std::array<std::thread *, Islands> islands;

    static void Evaluate(std::ranlux48_base *generator, PopulationT &population, std::deque<Agent *> &hof)
    {
        auto game = Game(generator);
        std::array<int32, Population> indices;
        std::iota(indices.begin(), indices.end(), 0);

        for (auto tournament : std::views::iota(0, EvalTournamentRep))
        {
            std::ranges::shuffle(indices, *generator);
            for (auto match : std::views::iota(0, Matches))
            {
                auto players = std::array<BaseAgent<State, Move> *, Players>();
                for (auto [i, j] : std::views::zip(std::views::iota(match * Players, (match + 1) * Players), std::views::iota(0, Players)))
                    players[j] = std::get<0>(population[indices[i]]);

                game.Play(players, EvalGameRep);

                auto scores = std::array<float64, Players>();
                for (int32 i = 0; i < Players; i++)
                    scores[i] = players[i]->FlushScore();

                for (int32 i = 0; i < Players; i++)
                    for (int32 j = 0; j < Players; j++)
                    {
                        if (i == j)
                            continue;
                        if (scores[i] <= scores[j])
                            std::get<1>(population[indices[match * Players + i]]).insert(indices[match * Players + j]);
                    }
            }
        }

        for (auto &[agent, champs, score] : population) // This tuple unpack is causing GDB to crash during dbg lmao
            for (auto champ : champs)
                std::get<2>(population[champ]) += 1.0 / champs.size();

        if constexpr (EvalHofSize > 0) // Hof evaluation
        {
            auto players = std::array<BaseAgent<State, Move> *, Players>();
            for (int32 i = 2; i < Players; i++)
                players[i] = new Dummy::Agent<State, Move>(generator);

            for (auto &boss : hof)
                for (auto &&[agent, champs, score] : population)
                {
                    players[0] = boss;
                    players[1] = agent;

                    game.Play(players, EvalGameRep);
                    players[0] = agent;
                    players[1] = boss;
                    game.Play(players, EvalGameRep);

                    if (boss->FlushScore() < agent->FlushScore())
                        score += EvalHofInfluence;
                }

            for (int32 i = 2; i < Players; i++)
                delete players[i];

            hof.push_back(new Agent(std::get<0>(*std::max_element(population.begin(), population.end(), [](auto &p1, auto &p2) { return std::get<2>(p1) < std::get<2>(p2); }))));
            if (hof.size() > EvalHofSize)
            {
                delete hof.front();
                hof.pop_front();
            }
        }
    }

    static void Cross(std::ranlux48_base *generator, PopulationT &population)
    {
        PopulationT population_next;
        if constexpr (CrossElitismSize == 0)
        {
            std::sort(population.begin(), population.end(), [](const auto &a, const auto &b) { return std::get<2>(a) > std::get<2>(b); });

            for (int32 i = 0; i < Population; i++)
            {
                auto &[agent, champs, score] = population_next[i];
                std::array<std::tuple<Agent *, std::set<int32>, float64>, CrossTournamentSize> sample_a, sample_b;
                std::ranges::sample(population, sample_a.begin(), CrossTournamentSize, *generator);
                std::ranges::sample(population, sample_b.begin(), CrossTournamentSize, *generator);
                auto parent_a = std::get<0>(sample_a[0]);
                auto parent_b = std::get<0>(sample_b[0]);

                agent = parent_a->Cross(parent_b);
                score = 0;
            }

            for (int32 i = 0; i < Population; i++)
                delete std::get<0>(population[i]);
        }
        if constexpr (CrossElitismSize > 0)
        {
            std::sort(population.begin(), population.end(), [](const auto &a, const auto &b) { return std::get<2>(a) > std::get<2>(b); });
            for (int32 i = 0; i < CrossElitismSize; i++)
                population_next[i] = std::tuple{std::get<0>(population[i]), std::set<int32>(), 0};

            for (int32 i = CrossElitismSize; i < Population; i++)
            {
                auto &[agent, champs, score] = population_next[i];
                std::array<std::tuple<Agent *, std::set<int32>, float64>, CrossTournamentSize> sample_a, sample_b;
                std::ranges::sample(population, sample_a.begin(), CrossTournamentSize, *generator);
                std::ranges::sample(population, sample_b.begin(), CrossTournamentSize, *generator);
                auto parent_a = std::get<0>(sample_a[0]);
                auto parent_b = std::get<0>(sample_b[0]);

                agent = parent_a->Cross(parent_b);
                score = 0;
            }

            for (int32 i = CrossElitismSize; i < Population; i++)
                delete std::get<0>(population[i]);
        }
        population = population_next;
    }

    static void Mutate(std::ranlux48_base *generator, PopulationT &population)
    {
        for (int32 i = CrossElitismSize; i < Population; i++)
            if (std::discrete_distribution<>{1.0 - MutateProbability, MutateProbability}(*generator))
                std::get<0>(population[i])->Mutate(MutateStrength);
    }

  public:
    Optimizer() {}

    ~Optimizer() {}

    auto Evolution(const std::initializer_list<int32> &epochs)
    {
        auto models = std::vector<std::invoke_result_t<decltype(&Agent::Save), Agent>>();
        models.reserve(epochs.size());

        auto device = new std::random_device();
        std::array<std::ranlux48_base *, Islands> generators;
        std::array<PopulationT, Islands> populations;
        std::array<std::deque<Agent *>, Islands> hofs;
        std::array<std::tuple<Agent *, std::set<int32>, float64>, Islands> best;
        for (auto &[agent, champs, score] : best)
            score = 0;

        for (int32 i = 0; i < Islands; i++)
        {
            generators[i] = new std::ranlux48_base((*device)());
            for (auto &[agent, champs, score] : populations[i])
            {
                agent = new Agent(generators[i]);
                champs = std::set<int32>();
                score = 0;
            }
        }

        int32 last_epoch = 0;
        for (int32 epoch : epochs)
        {
            int32 num_epoch = epoch - last_epoch;
            for (int32 i = 0; i < Islands; i++)
            {
                islands[i] = new std::thread(
                    [](int32 id, int32 last_epoch, int32 epochs, std::ranlux48_base *generator, PopulationT &population, std::deque<Agent *> &hof, Agent *&result)
                    {
                        for (int32 i = 0; i < epochs; i++)
                        {
                            Evaluate(generator, population, hof);
                            Cross(generator, population);
                            Mutate(generator, population);

                            if constexpr (Verbose)
                                std::cout << std::format("Thread {} epoch {} finished", id, last_epoch + i + 1) << std::endl;
                        }

                        Evaluate(generator, population, hof);
                        std::sort(population.begin(), population.end(), [](const auto &a, const auto &b) { return std::get<2>(a) > std::get<2>(b); });
                        result = std::get<0>(population[0]);

                        if constexpr (Verbose)
                            std::cout << std::format("Thread {} finished", id) << std::endl;
                    },
                    i,
                    last_epoch,
                    num_epoch,
                    generators[i],
                    std::ref(populations[i]),
                    std::ref(hofs[i]),
                    std::ref(std::get<0>(best[i])));
            }
            for (int32 i = 0; i < Islands; i++)
            {
                islands[i]->join();
                delete islands[i];
            }

            auto generator = new std::ranlux48_base((*device)());
            auto game = Game(generator);

            std::array<BaseAgent<State, Move> *, Players> players;
            for (int i = 2; i < Players; i++)
                players[i] = new Dummy::Agent<State, Move>(generator);

            for (int32 i = 0; i < Islands; i++)
                for (int32 j = i + 1; j < Islands; j++)
                {
                    players[0] = std::get<0>(best[i]);
                    players[1] = std::get<0>(best[j]);

                    game.Play(players, EvalGameRep);
                    players[0] = std::get<0>(best[j]);
                    players[1] = std::get<0>(best[i]);
                    game.Play(players, EvalGameRep);

                    std::array<float64, 2> scores = {players[0]->FlushScore(), players[1]->FlushScore()};

                    if (scores[0] <= scores[1])
                        std::get<1>(best[i]).insert(j);
                    if (scores[1] <= scores[1])
                        std::get<1>(best[i]).insert(j);
                }

            for (int32 i = 2; i < Players; i++)
                delete players[i];

            delete generator;

            for (auto &[agent, champs, score] : best)
                for (auto champ : champs)
                    std::get<2>(best[champ]) += 1.0 / champs.size();

            for (int32 i = 0; i < Islands; i++)
                for (auto &[agent, champs, score] : populations[i])
                {
                    champs = std::set<int32>();
                    score = 0;
                }

            models.push_back(std::get<0>(*std::max_element(best.begin(), best.end(), [](auto &p1, auto &p2) { return std::get<2>(p1) < std::get<2>(p2); }))->Save());

            if constexpr (Verbose)
                std::cout << "Batch done" << std::endl;

            last_epoch += num_epoch;
        }

        for (int32 i = 0; i < Islands; i++)
        {
            delete generators[i];
            for (auto &[agent, champs, score] : populations[i])
                delete agent;
            for (auto &agent : hofs[i])
                delete agent;
        }
        delete device;

        if constexpr (Verbose)
            std::cout << "Evolution done" << std::endl;

        return models;
    }
};
