#pragma once
#include <cstddef>
#include <cstdint>
#include <stdfloat>

using size_t = std::size_t;
using byte = std::byte;

using uint8 = std::uint8_t;
using uint16 = std::uint16_t;
using uint32 = std::uint32_t;
using uint64 = std::uint64_t;

using int8 = std::int8_t;
using int16 = std::int16_t;
using int32 = std::int32_t;
using int64 = std::int64_t;

using float16 = std::float16_t;
using float32 = std::float32_t;
using float64 = std::float64_t;
using float128 = std::float128_t;
