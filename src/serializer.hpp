#pragma once
#include <array>
#include <concepts>
#include <ranges>
#include <type_traits>
#include <vector>
#include "config.hpp"
#include "Eigen/Dense"

template <typename T, size_t N>
concept has_tuple_element = requires(T t) {
    typename std::tuple_element_t<N, std::remove_const_t<T>>;
    {
        get<N>(t)
    } -> std::convertible_to<const std::tuple_element_t<N, T> &>;
};

template <typename T>
concept tuple_like_v = !std::is_reference_v<T> && requires(T t) {
    typename std::tuple_size<T>::type;
    requires std::derived_from<std::tuple_size<T>, std::integral_constant<size_t, std::tuple_size_v<T>>>;
} && []<size_t... N>(std::index_sequence<N...>) { return (has_tuple_element<T, N> && ...); }(std::make_index_sequence<std::tuple_size_v<T>>());

template <typename T>
concept ITrivial = std::is_arithmetic_v<T> or std::is_enum_v<T>;

template <typename T>
concept IPointer = std::is_pointer_v<T> and std::is_default_constructible_v<T>;

template <typename T>
concept IArray = std::is_convertible_v<T, std::array<typename T::value_type, std::tuple_size<T>{}>>;

template <typename T>
concept ITuple = tuple_like_v<T>;

template <typename T>
concept ISerializable = requires { typename T::SerializeT; };

template <typename T>
concept IBuild = requires { typename T::BuildT; };

template <typename T>
concept IMatrix = std::is_convertible_v<T, Eigen::Matrix<typename T::Scalar, T::RowsAtCompileTime, T::ColsAtCompileTime>>;

class Serializer
{
  public:
    template <ITrivial T>
    static constexpr size_t Size(const T &obj)
    {
        return sizeof(T);
    }

    template <IPointer T>
    static constexpr size_t Size(const T &obj)
    {
        if (obj)
            return sizeof(bool) + Size<std::remove_pointer_t<T>>(*obj);
        return sizeof(bool);
    }

    template <IArray T, typename V = typename T::value_type>
    static constexpr size_t Size(const T &obj)
    {
        size_t s = 0;
        for (const auto &e : obj)
            s += Size<V>(e);
        return s;
    }

    template <ITuple T, size_t I = 0, typename V = std::tuple_element_t<I, T>>
    requires(!IArray<T>)
    static constexpr size_t Size(const T &obj)
    {
        if constexpr (I + 1 != std::tuple_size<T>{})
        {
            return Size<V>(std::get<I>(obj)) + Size<T, I + 1>(obj);
        }
        if constexpr (I + 1 == std::tuple_size<T>{})
        {
            return Size<V>(std::get<I>(obj));
        }
    }

    template <ISerializable T>
    static constexpr size_t Size(const T &obj)
    {
        return Size<typename T::SerializeT>(obj.Serialize());
    }

    template <IBuild T>
    static constexpr size_t Size(const T &obj)
    {
        return Size<typename T::BuildT>(obj.Serialize());
    }

    template <IMatrix T, typename Scalar = typename T::Scalar>
    static constexpr size_t Size(const T &obj)
    {
        size_t s = 0;
        for (const auto &e : obj.reshaped())
            s += Size<Scalar>(e);
        return s;
    }

  private:
    template <ITrivial T>
    static void Parse(const T &obj, byte *&buffer)
    {
        std::memcpy(buffer, reinterpret_cast<const byte *>(&obj), sizeof(T));
        buffer += sizeof(T);
    }

    template <IPointer T>
    static void Parse(const T &obj, byte *&buffer)
    {
        bool ptr = obj;
        Parse<bool>(ptr, buffer);
        if (ptr)
            Parse<std::remove_pointer_t<T>>(*obj, buffer);
    }

    template <IArray T, typename V = typename T::value_type, size_t S = std::tuple_size<T>{}>
    static void Parse(const T &obj, byte *&buffer)
    {
        for (size_t i = 0; i < S; i++)
            Parse<V>(obj[i], buffer);
    }

    template <ITuple T, size_t I = 0, typename V = std::tuple_element_t<I, T>>
    requires(!IArray<T>)
    static void Parse(const T &obj, byte *&buffer)
    {
        if constexpr (I + 1 != std::tuple_size<T>{})
        {
            Parse<V>(std::get<I>(obj), buffer);
            Parse<T, I + 1>(obj, buffer);
        }
        if constexpr (I + 1 == std::tuple_size<T>{})
        {
            Parse<V>(std::get<I>(obj), buffer);
        }
    }

    template <ISerializable T>
    static void Parse(const T &obj, byte *&buffer)
    {
        Parse<typename T::SerializeT>(obj.Serialize(), buffer);
    }

    template <IBuild T>
    static void Parse(const T &obj, byte *&buffer)
    {
        Parse<typename T::BuildT>(obj.Serialize(), buffer);
    }

    template <IMatrix T, typename Scalar = typename T::Scalar, int32 Rows = T::RowsAtCompileTime, int32 Cols = T::ColsAtCompileTime>
    static void Parse(const T &obj, byte *&buffer)
    {
        for (auto &&e : obj.reshaped())
            Parse<Scalar>(e, buffer);
    }

    template <ITrivial T>
    static T Decode(const byte *&buffer)
    {
        auto obj = T(*reinterpret_cast<const T *>(buffer));
        buffer += sizeof(T);
        return obj;
    }

    template <IPointer T, typename Y = std::remove_pointer_t<T>>
    static T Decode(const byte *&buffer)
    {
        bool ptr = Decode<bool>(buffer);
        if (ptr)
        {
            T obj = new Y;
            *obj = Decode<Y>(buffer);
            return obj;
        }
        return nullptr;
    }

    template <IArray T, typename V = typename T::value_type, size_t S = std::tuple_size<T>{}>
    static T Decode(const byte *&buffer)
    {
        T obj;
        for (size_t i = 0; i < S; i++)
            obj[i] = Decode<V>(buffer);
        return obj;
    }

    template <ITuple T, size_t I = 0, typename V = std::tuple_element_t<I, T>>
    requires(!IArray<T>)
    static auto Decode(const byte *&buffer)
    {
        if constexpr (I + 1 != std::tuple_size<T>{})
        {
            auto head = std::tuple{Decode<V>(buffer)};
            auto tail = Decode<T, I + 1>(buffer);
            return std::tuple_cat(head, tail);
        }
        if constexpr (I + 1 == std::tuple_size<T>{})
        {
            return std::tuple{Decode<V>(buffer)};
        }
    }

    template <ISerializable T>
    static T Decode(const byte *&buffer)
    {
        T obj;
        obj.Serialize() = Decode<typename T::SerializeT>(buffer);
        return obj;
    }

    template <IBuild T>
    static T Decode(const byte *&buffer)
    {
        T obj;
        obj.Build(Decode<typename T::BuildT>(buffer));
        return obj;
    }

    template <IMatrix T, typename Scalar = typename T::Scalar, int32 Rows = T::RowsAtCompileTime, int32 Cols = T::ColsAtCompileTime>
    static T Decode(const byte *&buffer)
    {
        T obj;
        for (auto &&e : obj.reshaped())
            e = Decode<Scalar>(buffer);
        return obj;
    }

  public:
    template <typename T>
    static auto Save(const T &obj)
    {
        std::vector<byte> buffer;
        buffer.resize(Size<T>(obj));
        byte *ptr = buffer.data();
        Parse<T>(obj, ptr);
        return buffer;
    }

    template <typename T>
    static auto Load(const byte *data)
    {
        return Decode<T>(data);
    }
};
