#pragma once
#include "types.hpp"

#define EIGEN_USE_MKL_ALL
#define EIGEN_STACK_ALLOCATION_LIMIT 8388608

constexpr bool Draw = false;
constexpr bool Verbose = true;
constexpr int32 WindowSize = 512;
constexpr uint64 Tickrate = 8;
