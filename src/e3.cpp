#include <filesystem>
#include <format>
#include <fstream>
#include <iostream>
#include <ranges>
#include <set>
#include <vector>
#include "agents/dummy.hpp"
#include "agents/heuristic.hpp"
#include "agents/human.hpp"
#include "agents/neural.hpp"
#include "agents/random.hpp"
#include "agents/reinforced.hpp"
#include "agents/tree.hpp"
#include "games/bomb.hpp"
#include "games/catch.hpp"
#include "games/curve.hpp"
#include "games/digger.hpp"
#include "optimizer.hpp"

int main()
{
    auto epochs = std::initializer_list<int32>{1, 2, 5, 10, 20, 50, 100, 200, 300};
    using Game = Catch::Game<4, 8, 4, float32, 50>;
    std::filesystem::create_directories("models");

    {
        auto optimizer = new Optimizer<Game, Tree::Agent<Game::StateT, Game::MoveT, 8>, 12, 1024, 4, 4, 16, 1.0f64, 4, 4, 0.1f64, 0.01f64>();
        auto models = optimizer->Evolution(epochs);
        std::filesystem::create_directories("models/tree");
        for (const auto &[model, epoch] : std::views::zip(models, epochs))
        {
            std::fstream file(std::format("models/tree/model_{}.bin", epoch), std::ios::out | std::ios::binary);
            file.write((char *)(model.data()), model.size());
            file.close();
        }
        delete optimizer;
    }

    {
        auto optimizer = new Optimizer<Game, Neural::Agent<Game::StateT, Game::MoveT, Neural::LinearLayer, Neural::ReLULayer<16>>, 12, 1024, 4, 4, 16, 1.0f64, 4, 4, 0.1f64, 0.01f64>();
        auto models = optimizer->Evolution(epochs);
        std::filesystem::create_directories("models/relu");
        for (const auto &[model, epoch] : std::views::zip(models, epochs))
        {
            std::fstream file(std::format("models/relu/model_{}.bin", epoch), std::ios::out | std::ios::binary);
            file.write((char *)(model.data()), model.size());
            file.close();
        }
        delete optimizer;
    }

    {
        auto optimizer = new Optimizer<Game, Neural::Agent<Game::StateT, Game::MoveT, Neural::LinearLayer, Neural::TanhLayer<16>>, 12, 1024, 4, 4, 16, 1.0f64, 4, 4, 0.1f64, 0.01f64>();
        auto models = optimizer->Evolution(epochs);
        std::filesystem::create_directories("models/tanh");
        for (const auto &[model, epoch] : std::views::zip(models, epochs))
        {
            std::fstream file(std::format("models/tanh/model_{}.bin", epoch), std::ios::out | std::ios::binary);
            file.write((char *)(model.data()), model.size());
            file.close();
        }
        delete optimizer;
    }

    {
        auto optimizer = new Optimizer<Game, Neural::Agent<Game::StateT, Game::MoveT, Neural::LinearLayer, Neural::LogisticLayer<16>>, 12, 1024, 4, 4, 16, 1.0f64, 4, 4, 0.1f64, 0.01f64>();
        auto models = optimizer->Evolution(epochs);
        std::filesystem::create_directories("models/logistic");
        for (const auto &[model, epoch] : std::views::zip(models, epochs))
        {
            std::fstream file(std::format("models/logistic/model_{}.bin", epoch), std::ios::out | std::ios::binary);
            file.write((char *)(model.data()), model.size());
            file.close();
        }
        delete optimizer;
    }

    {
        auto optimizer = new Optimizer<Game, Neural::Agent<Game::StateT, Game::MoveT, Neural::LinearLayer, Neural::ReLURecurrentLayer<16>>, 12, 1024, 4, 4, 16, 1.0f64, 4, 4, 0.1f64, 0.01f64>();
        auto models = optimizer->Evolution(epochs);
        std::filesystem::create_directories("models/relu_rec");
        for (const auto &[model, epoch] : std::views::zip(models, epochs))
        {
            std::fstream file(std::format("models/relu_rec/model_{}.bin", epoch), std::ios::out | std::ios::binary);
            file.write((char *)(model.data()), model.size());
            file.close();
        }
        delete optimizer;
    }

    {
        auto optimizer = new Optimizer<Game, Neural::Agent<Game::StateT, Game::MoveT, Neural::LinearLayer, Neural::GRULayer<16>>, 12, 1024, 4, 4, 16, 1.0f64, 4, 4, 0.1f64, 0.01f64>();
        auto models = optimizer->Evolution(epochs);
        std::filesystem::create_directories("models/gru");
        for (const auto &[model, epoch] : std::views::zip(models, epochs))
        {
            std::fstream file(std::format("models/gru/model_{}.bin", epoch), std::ios::out | std::ios::binary);
            file.write((char *)(model.data()), model.size());
            file.close();
        }
        delete optimizer;
    }

    {
        auto optimizer = new Optimizer<Game, Neural::Agent<Game::StateT, Game::MoveT, Neural::LinearLayer, Neural::LSTMLayer<16>>, 12, 1024, 4, 4, 16, 1.0f64, 4, 4, 0.1f64, 0.01f64>();
        auto models = optimizer->Evolution(epochs);
        std::filesystem::create_directories("models/lstm");
        for (const auto &[model, epoch] : std::views::zip(models, epochs))
        {
            std::fstream file(std::format("models/lstm/model_{}.bin", epoch), std::ios::out | std::ios::binary);
            file.write((char *)(model.data()), model.size());
            file.close();
        }
        delete optimizer;
    }
}
