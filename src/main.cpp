#include <filesystem>
#include <format>
#include <fstream>
#include <iostream>
#include <ranges>
#include <set>
#include <vector>
#include "agents/dummy.hpp"
#include "agents/heuristic.hpp"
#include "agents/human.hpp"
#include "agents/neural.hpp"
#include "agents/random.hpp"
#include "agents/reinforced.hpp"
#include "agents/tree.hpp"
#include "games/bomb.hpp"
#include "games/catch.hpp"
#include "games/curve.hpp"
#include "games/digger.hpp"
#include "optimizer.hpp"

int main()
{
    auto epochs = std::initializer_list<int32>{1, 2, 5, 10, 20, 50, 100, 200, 300};

    auto generator = new std::ranlux48_base(2137);
    auto game = Digger::Game<4, 20, 6, float32, 0, 30, 1, 10, 1>(generator);

    // VS OTHERS
    // for (int32 epoch : epochs)
    // {
    //     auto p1 = new Tree::Agent<decltype(game)::StateT, decltype(game)::MoveT, 8>(generator);
    //     auto p2 = new Neural::Agent<decltype(game)::StateT, decltype(game)::MoveT, Neural::LinearLayer, Neural::ReLULayer<16>>(generator);
    //     auto p3 = new Dummy::Agent<decltype(game)::StateT, decltype(game)::MoveT>(generator);
    //     auto p4 = new Dummy::Agent<decltype(game)::StateT, decltype(game)::MoveT>(generator);

    //     std::fstream data_1(std::format("models/tree/model_{}.bin", epoch), std::ios::in | std::ios::binary);
    //     std::vector<byte> model_1;
    //     size_t size_1 = std::filesystem::file_size(std::format("models/tree/model_{}.bin", epoch));
    //     model_1.resize(size_1);
    //     data_1.read((char *)(model_1.data()), size_1);
    //     p1->Load(model_1.data());

    //     std::fstream data_2(std::format("models/relu/model_{}.bin", epoch), std::ios::in | std::ios::binary);
    //     std::vector<byte> model_2;
    //     size_t size_2 = std::filesystem::file_size(std::format("models/relu/model_{}.bin", epoch));
    //     model_2.resize(size_2);
    //     data_2.read((char *)(model_2.data()), size_2);
    //     p2->Load(model_2.data());

    //     generator->seed(2137);
    //     game.Play({p1, p2, p3, p4}, 10000);
    //     generator->seed(2137);
    //     game.Play({p2, p1, p3, p4}, 10000);

    //     std::cout << p1->FlushScore() - p2->FlushScore() << "\n";

    //     delete p1;
    //     delete p2;
    //     delete p3;
    //     delete p4;
    // }

    // VS HEURISTIC, DUMMY AND RANDOM
    // for (int32 epoch : epochs)
    // {
    //     auto p1 = new Neural::Agent<decltype(game)::StateT, decltype(game)::MoveT, Neural::LinearLayer, Neural::GRULayer<16>>(generator);
    //     auto p2 = new Random::Agent<decltype(game)::StateT, decltype(game)::MoveT>(generator);
    //     auto p3 = new Dummy::Agent<decltype(game)::StateT, decltype(game)::MoveT>(generator);
    //     auto p4 = new Dummy::Agent<decltype(game)::StateT, decltype(game)::MoveT>(generator);

    //     std::fstream data_1(std::format("models/gru/model_{}.bin", epoch), std::ios::in | std::ios::binary);
    //     std::vector<byte> model_1;
    //     size_t size_1 = std::filesystem::file_size(std::format("models/gru/model_{}.bin", epoch));
    //     model_1.resize(size_1);
    //     data_1.read((char *)(model_1.data()), size_1);
    //     p1->Load(model_1.data());

    //     generator->seed(2137);
    //     game.Play({p1, p2, p3, p4}, 10000);
    //     generator->seed(2137);
    //     game.Play({p2, p1, p3, p4}, 10000);

    //     std::cout << p1->FlushScore() - p2->FlushScore() << "\n";

    //     delete p1;
    //     delete p2;
    //     delete p3;
    //     delete p4;
    // }

    // VS REINFORCED
    // for (int32 epoch : epochs)
    // {
    //     auto p1 = new Neural::Agent<decltype(game)::StateT, decltype(game)::MoveT, Neural::LinearLayer, Neural::GRULayer<16>>(generator);
    //     auto p2 = new Reinforced::Agent<decltype(game)::StateT, decltype(game)::MoveT, false>(generator);
    //     auto p3 = new Dummy::Agent<decltype(game)::StateT, decltype(game)::MoveT>(generator);
    //     auto p4 = new Dummy::Agent<decltype(game)::StateT, decltype(game)::MoveT>(generator);

    //     std::fstream data_1(std::format("models/gru/model_{}.bin", epoch), std::ios::in | std::ios::binary);
    //     std::vector<byte> model_1;
    //     size_t size_1 = std::filesystem::file_size(std::format("models/gru/model_{}.bin", epoch));
    //     model_1.resize(size_1);
    //     data_1.read((char *)(model_1.data()), size_1);
    //     p1->Load(model_1.data());

    //     std::fstream data_2("models/reinforced.bin", std::ios::in | std::ios::binary);
    //     std::vector<byte> model_2;
    //     size_t size_2 = std::filesystem::file_size("models/reinforced.bin");
    //     model_2.resize(size_2);
    //     data_2.read((char *)(model_2.data()), size_2);
    //     p2->Load(model_2.data());

    //     generator->seed(2137);
    //     game.Play({p1, p2, p3, p4}, 10000);
    //     generator->seed(2137);
    //     game.Play({p2, p1, p3, p4}, 10000);

    //     std::cout << p1->FlushScore() - p2->FlushScore() << "\n";

    //     delete p1;
    //     delete p2;
    //     delete p3;
    //     delete p4;
    // }

    // VS SELF
    // for (auto &&[epoch, epoch_prev] : std::views::zip(std::vector<int32>(epochs.begin() + 1, epochs.end()), epochs))
    // {
    //     auto p1 = new Neural::Agent<decltype(game)::StateT, decltype(game)::MoveT, Neural::LinearLayer, Neural::GRULayer<16>>(generator);
    //     auto p2 = new Neural::Agent<decltype(game)::StateT, decltype(game)::MoveT, Neural::LinearLayer, Neural::GRULayer<16>>(generator);
    //     auto p3 = new Dummy::Agent<decltype(game)::StateT, decltype(game)::MoveT>(generator);
    //     auto p4 = new Dummy::Agent<decltype(game)::StateT, decltype(game)::MoveT>(generator);

    //     std::fstream data_1(std::format("models/gru/model_{}.bin", epoch), std::ios::in | std::ios::binary);
    //     std::vector<byte> model_1;
    //     size_t size_1 = std::filesystem::file_size(std::format("models/gru/model_{}.bin", epoch));
    //     model_1.resize(size_1);
    //     data_1.read((char *)(model_1.data()), size_1);
    //     p1->Load(model_1.data());

    //     std::fstream data_2(std::format("models/gru/model_{}.bin", epoch_prev), std::ios::in | std::ios::binary);
    //     std::vector<byte> model_2;
    //     size_t size_2 = std::filesystem::file_size(std::format("models/gru/model_{}.bin", epoch_prev));
    //     model_2.resize(size_2);
    //     data_2.read((char *)(model_2.data()), size_2);
    //     p2->Load(model_2.data());

    //     generator->seed(2137);
    //     game.Play({p1, p2, p3, p4}, 10000);
    //     generator->seed(2137);
    //     game.Play({p2, p1, p3, p4}, 10000);

    //     std::cout << p1->FlushScore() - p2->FlushScore() << "\n";

    //     delete p1;
    //     delete p2;
    //     delete p3;
    //     delete p4;
    // }
}
