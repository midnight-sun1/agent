#pragma once
#include <algorithm>
#include <random>
#include <ranges>
#include <tuple>
#include <type_traits>
#include "Eigen/Dense"
#include "base.hpp"
#include "serializer.hpp"

namespace Neural
{

template <typename Scalar, int32 Neurons>
struct Linear
{
    auto operator()(const Eigen::Vector<Scalar, Neurons> &input)
    {
        return input;
    }
};

template <typename Scalar, int32 Neurons>
struct Tanh
{
    auto operator()(const Eigen::Vector<Scalar, Neurons> &input)
    {
        return input.array().tanh();
    }
};

template <typename Scalar, int32 Neurons>
struct ReLU
{
    auto operator()(const Eigen::Vector<Scalar, Neurons> &input)
    {
        return input.array().unaryExpr([](const auto &x) { return std::max((decltype(x))0, x); });
    }
};

template <typename Scalar, int32 Neurons>
struct Logistic
{
    auto operator()(const Eigen::Vector<Scalar, Neurons> &input)
    {
        return input.array().unaryExpr([](const auto &x) { return std::exp(x) / (std::exp(x) + (decltype(x))1); });
    }
};

template <typename Scalar, int32 Neurons>
struct Softmax
{
    auto operator()(const Eigen::Vector<Scalar, Neurons> &input)
    {
        auto exp = input.array().unaryExpr([](const auto &x) { return std::exp(x); });
        auto sum = std::accumulate(exp.begin(), exp.end(), 0);

        return exp.unaryExpr([&sum](const auto &x) { return x / sum; });
    }
};

template <int32 Neurons, template <typename, int32> typename Activation>
struct SimpleLayerTp
{
    static constexpr int32 NeuronsI = Neurons;

    template <typename Scalar, int32 Inputs>
    struct Layer
    {
        Eigen::Matrix<Scalar, Neurons, Inputs> weight;
        Eigen::Vector<Scalar, Neurons> bias;

        using SerializeT = std::tuple<decltype(weight), decltype(bias)>;

        Layer() {}

        Layer(std::ranlux48_base *generator)
        {
            weight = Eigen::Matrix<Scalar, Neurons, Inputs>::NullaryExpr(Neurons, Inputs, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            bias = Eigen::Vector<Scalar, Neurons>::NullaryExpr(Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
        }

        const Eigen::Vector<Scalar, Neurons> operator()(const Eigen::Vector<Scalar, Inputs> &input)
        {
            return Activation<Scalar, Neurons>()(weight * input + bias);
        }

        void Reset() {}

        template <size_t I = 0>
        void Mutate(auto &&params, const float64 &strength, std::ranlux48_base *generator)
        {
            if constexpr (I + 1 != std::tuple_size<SerializeT>{})
            {
                for (auto &param : std::get<I>(params).reshaped())
                    if (std::discrete_distribution<>{1.0f64 - strength, strength}(*generator))
                        param += std::normal_distribution<Scalar>(0, 1)(*generator);

                Mutate<I + 1>(params, strength, generator);
            }
            if constexpr (I + 1 == std::tuple_size<SerializeT>{})
            {
                for (auto &param : std::get<I>(params).reshaped())
                    if (std::discrete_distribution<>{1.0f64 - strength, strength}(*generator))
                        param += std::normal_distribution<Scalar>(0, 1)(*generator);
            }
        }

        void Mutate(const float64 &strength, std::ranlux48_base *generator)
        {
            Mutate(Serialize(), strength, generator);
        }

        template <size_t I = 0>
        void Cross(const auto params_a, const auto params_b, auto params_c, std::ranlux48_base *generator) const
        {
            auto &m_a = std::get<I>(params_a);
            auto &m_b = std::get<I>(params_b);
            auto &m_c = std::get<I>(params_c);

            if constexpr (I + 1 != std::tuple_size<SerializeT>{})
            {
                for (auto &&[a, b, c] : std::views::zip(m_a.reshaped(), m_b.reshaped(), m_c.reshaped()))
                {
                    if (std ::discrete_distribution<>{1, 1}(*generator))
                        c = a;
                    else
                        c = b;
                }

                Cross<I + 1>(params_a, params_b, params_c, generator);
            }
            if constexpr (I + 1 == std::tuple_size<SerializeT>{})
            {
                for (auto &&[a, b, c] : std::views::zip(m_a.reshaped(), m_b.reshaped(), m_c.reshaped()))
                {
                    if (std ::discrete_distribution<>{1, 1}(*generator))
                        c = a;
                    else
                        c = b;
                }
            }
        }

        Layer Cross(const Layer &other, std::ranlux48_base *generator) const
        {
            Layer layer;
            Cross(Serialize(), other.Serialize(), layer.Serialize(), generator);
            return layer;
        }

        const auto Serialize() const
        {
            return std::tie(weight, bias);
        }

        auto Serialize()
        {
            return std::tie(weight, bias);
        }
    };
};

template <int32 Neurons, template <typename, int32> typename Activation>
struct RecurrentLayerTp
{
    static constexpr int32 NeuronsI = Neurons;

    template <typename Scalar, int32 Inputs>
    struct Layer
    {
        Eigen::Matrix<Scalar, Neurons, Inputs> weight_input;
        Eigen::Matrix<Scalar, Neurons, Neurons> weight_hidden;
        Eigen::Vector<Scalar, Neurons> bias, hidden;

        using SerializeT = std::tuple<decltype(weight_input), decltype(weight_hidden), decltype(bias)>;

        Layer() {}

        Layer(std::ranlux48_base *generator)
        {
            weight_input = Eigen::Matrix<Scalar, Neurons, Inputs>::NullaryExpr(Neurons, Inputs, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            weight_hidden = Eigen::Matrix<Scalar, Neurons, Neurons>::NullaryExpr(Neurons, Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            bias = Eigen::Vector<Scalar, Neurons>::NullaryExpr(Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
        }

        const Eigen::Vector<Scalar, Neurons> operator()(const Eigen::Vector<Scalar, Inputs> &input)
        {
            hidden = Activation<Scalar, Neurons>()(weight_input * input + weight_hidden * hidden + bias);
            return hidden;
        }

        void Reset()
        {
            hidden = Eigen::Vector<Scalar, Neurons>::Zero();
        }

        template <size_t I = 0>
        void Mutate(auto &&params, const float64 &strength, std::ranlux48_base *generator)
        {
            if constexpr (I + 1 != std::tuple_size<SerializeT>{})
            {
                for (auto &param : std::get<I>(params).reshaped())
                    if (std::discrete_distribution<>{1.0f64 - strength, strength}(*generator))
                        param += std::normal_distribution<Scalar>(0, 1)(*generator);

                Mutate<I + 1>(params, strength, generator);
            }
            if constexpr (I + 1 == std::tuple_size<SerializeT>{})
            {
                for (auto &param : std::get<I>(params).reshaped())
                    if (std::discrete_distribution<>{1.0f64 - strength, strength}(*generator))
                        param += std::normal_distribution<Scalar>(0, 1)(*generator);
            }
        }

        void Mutate(const float64 &strength, std::ranlux48_base *generator)
        {
            Mutate(Serialize(), strength, generator);
        }

        template <size_t I = 0>
        void Cross(const auto params_a, const auto params_b, auto params_c, std::ranlux48_base *generator) const
        {
            auto &m_a = std::get<I>(params_a);
            auto &m_b = std::get<I>(params_b);
            auto &m_c = std::get<I>(params_c);

            if constexpr (I + 1 != std::tuple_size<SerializeT>{})
            {
                for (auto &&[a, b, c] : std::views::zip(m_a.reshaped(), m_b.reshaped(), m_c.reshaped()))
                {
                    if (std ::discrete_distribution<>{1, 1}(*generator))
                        c = a;
                    else
                        c = b;
                }

                Cross<I + 1>(params_a, params_b, params_c, generator);
            }
            if constexpr (I + 1 == std::tuple_size<SerializeT>{})
            {
                for (auto &&[a, b, c] : std::views::zip(m_a.reshaped(), m_b.reshaped(), m_c.reshaped()))
                {
                    if (std ::discrete_distribution<>{1, 1}(*generator))
                        c = a;
                    else
                        c = b;
                }
            }
        }

        Layer Cross(const Layer &other, std::ranlux48_base *generator) const
        {
            Layer layer;
            Cross(Serialize(), other.Serialize(), layer.Serialize(), generator);
            return layer;
        }

        const auto Serialize() const
        {
            return std::tie(weight_input, weight_hidden, bias);
        }

        auto Serialize()
        {
            return std::tie(weight_input, weight_hidden, bias);
        }
    };
};

template <int32 Neurons>
using LinearLayer = SimpleLayerTp<Neurons, Linear>;

template <int32 Neurons>
using TanhLayer = SimpleLayerTp<Neurons, Tanh>;

template <int32 Neurons>
using ReLULayer = SimpleLayerTp<Neurons, ReLU>;

template <int32 Neurons>
using LogisticLayer = SimpleLayerTp<Neurons, Logistic>;

template <int32 Neurons>
using SoftmaxLayer = SimpleLayerTp<Neurons, Softmax>;

template <int32 Neurons>
using LinearRecurrentLayer = RecurrentLayerTp<Neurons, Linear>;

template <int32 Neurons>
using TanhRecurrentLayer = RecurrentLayerTp<Neurons, Tanh>;

template <int32 Neurons>
using ReLURecurrentLayer = RecurrentLayerTp<Neurons, ReLU>;

template <int32 Neurons>
using LogisticRecurrentLayer = RecurrentLayerTp<Neurons, Logistic>;

template <int32 Neurons>
using SoftmaxRecurrentLayer = RecurrentLayerTp<Neurons, Softmax>;

template <int32 Neurons>
struct GRULayer
{
    static constexpr int32 NeuronsI = Neurons;

    template <typename Scalar, int32 Inputs>
    struct Layer
    {
        Eigen::Matrix<Scalar, Neurons, Inputs> w_z, w_r, w_h;
        Eigen::Matrix<Scalar, Neurons, Neurons> u_z, u_r, u_h;
        Eigen::Vector<Scalar, Neurons> h, b_z, b_r, b_h;

        using SerializeT = std::tuple<decltype(w_z), decltype(w_r), decltype(w_h), decltype(u_z), decltype(u_r), decltype(u_h), decltype(b_z), decltype(b_r), decltype(b_h)>;

        Layer() {}

        Layer(std::ranlux48_base *generator)
        {
            w_z = Eigen::Matrix<Scalar, Neurons, Inputs>::NullaryExpr(Neurons, Inputs, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            w_r = Eigen::Matrix<Scalar, Neurons, Inputs>::NullaryExpr(Neurons, Inputs, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            w_h = Eigen::Matrix<Scalar, Neurons, Inputs>::NullaryExpr(Neurons, Inputs, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            u_z = Eigen::Matrix<Scalar, Neurons, Neurons>::NullaryExpr(Neurons, Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            u_r = Eigen::Matrix<Scalar, Neurons, Neurons>::NullaryExpr(Neurons, Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            u_h = Eigen::Matrix<Scalar, Neurons, Neurons>::NullaryExpr(Neurons, Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            b_z = Eigen::Vector<Scalar, Neurons>::NullaryExpr(Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            b_r = Eigen::Vector<Scalar, Neurons>::NullaryExpr(Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            b_h = Eigen::Vector<Scalar, Neurons>::NullaryExpr(Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
        }

        const Eigen::Vector<Scalar, Neurons> operator()(const Eigen::Vector<Scalar, Inputs> &x)
        {
            auto z = Logistic<Scalar, Neurons>()(w_z * x + u_z * h + b_z);
            auto r = Logistic<Scalar, Neurons>()(w_r * x + u_r * h + b_r);
            auto h_hat = Tanh<Scalar, Neurons>()(w_h * x + (r.array() * (u_h * h).array()).matrix() + b_h);

            h = z.array() * h.array() + (1 - z).array() * h_hat.array();

            return h;
        }

        void Reset()
        {
            h = Eigen::Vector<Scalar, Neurons>::Zero();
        }

        template <size_t I = 0>
        void Mutate(auto params, const float64 &strength, std::ranlux48_base *generator)
        {
            if constexpr (I + 1 != std::tuple_size<SerializeT>{})
            {
                for (auto &param : std::get<I>(params).reshaped())
                    if (std::discrete_distribution<>{1.0f64 - strength, strength}(*generator))
                        param += std::normal_distribution<Scalar>(0, 1)(*generator);

                Mutate<I + 1>(params, strength, generator);
            }
            if constexpr (I + 1 == std::tuple_size<SerializeT>{})
            {
                for (auto &param : std::get<I>(params).reshaped())
                    if (std::discrete_distribution<>{1.0f64 - strength, strength}(*generator))
                        param += std::normal_distribution<Scalar>(0, 1)(*generator);
            }
        }

        void Mutate(const float64 &strength, std::ranlux48_base *generator)
        {
            Mutate(Serialize(), strength, generator);
        }

        template <size_t I = 0>
        void Cross(const auto params_a, const auto params_b, auto params_c, std::ranlux48_base *generator) const
        {
            auto &m_a = std::get<I>(params_a);
            auto &m_b = std::get<I>(params_b);
            auto &m_c = std::get<I>(params_c);

            if constexpr (I + 1 != std::tuple_size<SerializeT>{})
            {
                for (auto &&[a, b, c] : std::views::zip(m_a.reshaped(), m_b.reshaped(), m_c.reshaped()))
                {
                    if (std ::discrete_distribution<>{1, 1}(*generator))
                        c = a;
                    else
                        c = b;
                }

                Cross<I + 1>(params_a, params_b, params_c, generator);
            }
            if constexpr (I + 1 == std::tuple_size<SerializeT>{})
            {
                for (auto &&[a, b, c] : std::views::zip(m_a.reshaped(), m_b.reshaped(), m_c.reshaped()))
                {
                    if (std ::discrete_distribution<>{1, 1}(*generator))
                        c = a;
                    else
                        c = b;
                }
            }
        }

        Layer Cross(const Layer &other, std::ranlux48_base *generator) const
        {
            Layer layer;
            Cross(Serialize(), other.Serialize(), layer.Serialize(), generator);
            return layer;
        }

        const auto Serialize() const
        {
            return std::tie(w_z, w_r, w_h, u_z, u_r, u_h, b_z, b_r, b_h);
        }

        auto Serialize()
        {
            return std::tie(w_z, w_r, w_h, u_z, u_r, u_h, b_z, b_r, b_h);
        }
    };
};

template <int32 Neurons>
struct LSTMLayer
{
    static constexpr int32 NeuronsI = Neurons;

    template <typename Scalar, int32 Inputs>
    struct Layer
    {
        Eigen::Matrix<Scalar, Neurons, Inputs> w_f, w_i, w_o, w_c;
        Eigen::Matrix<Scalar, Neurons, Neurons> u_f, u_i, u_o, u_c;
        Eigen::Vector<Scalar, Neurons> h, c, b_f, b_i, b_o, b_c;

        using SerializeT = std::
            tuple<decltype(w_f), decltype(w_i), decltype(w_o), decltype(w_c), decltype(u_f), decltype(u_i), decltype(u_o), decltype(u_c), decltype(b_f), decltype(b_i), decltype(b_o), decltype(b_c)>;

        Layer() {}

        Layer(std::ranlux48_base *generator)
        {
            w_f = Eigen::Matrix<Scalar, Neurons, Inputs>::NullaryExpr(Neurons, Inputs, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            w_i = Eigen::Matrix<Scalar, Neurons, Inputs>::NullaryExpr(Neurons, Inputs, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            w_o = Eigen::Matrix<Scalar, Neurons, Inputs>::NullaryExpr(Neurons, Inputs, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            w_c = Eigen::Matrix<Scalar, Neurons, Inputs>::NullaryExpr(Neurons, Inputs, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            u_f = Eigen::Matrix<Scalar, Neurons, Neurons>::NullaryExpr(Neurons, Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            u_i = Eigen::Matrix<Scalar, Neurons, Neurons>::NullaryExpr(Neurons, Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            u_o = Eigen::Matrix<Scalar, Neurons, Neurons>::NullaryExpr(Neurons, Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            u_c = Eigen::Matrix<Scalar, Neurons, Neurons>::NullaryExpr(Neurons, Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            b_f = Eigen::Vector<Scalar, Neurons>::NullaryExpr(Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            b_i = Eigen::Vector<Scalar, Neurons>::NullaryExpr(Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            b_o = Eigen::Vector<Scalar, Neurons>::NullaryExpr(Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
            b_c = Eigen::Vector<Scalar, Neurons>::NullaryExpr(Neurons, [&generator]() { return std::normal_distribution<Scalar>(0, 1)(*generator); });
        }

        const Eigen::Vector<Scalar, Neurons> operator()(const Eigen::Vector<Scalar, Inputs> &x)
        {
            auto f = Logistic<Scalar, Neurons>()(w_f * x + u_f * h + b_f);
            auto i = Logistic<Scalar, Neurons>()(w_i * x + u_i * h + b_i);
            auto o = Logistic<Scalar, Neurons>()(w_o * x + u_o * h + b_o);
            auto c_hat = Tanh<Scalar, Neurons>()(w_c * x + u_c * h + b_c);

            c = f.array() * c.array() + i.array() * c_hat.array();
            h = o.array() * Tanh<Scalar, Neurons>()(c).array();

            return h;
        }

        void Reset()
        {
            h = Eigen::Vector<Scalar, Neurons>::Zero();
            c = Eigen::Vector<Scalar, Neurons>::Zero();
        }

        template <size_t I = 0>
        void Mutate(auto params, const float64 &strength, std::ranlux48_base *generator)
        {
            if constexpr (I + 1 != std::tuple_size<SerializeT>{})
            {
                for (auto &param : std::get<I>(params).reshaped())
                    if (std::discrete_distribution<>{1.0f64 - strength, strength}(*generator))
                        param += std::normal_distribution<Scalar>(0, 1)(*generator);

                Mutate<I + 1>(params, strength, generator);
            }
            if constexpr (I + 1 == std::tuple_size<SerializeT>{})
            {
                for (auto &param : std::get<I>(params).reshaped())
                    if (std::discrete_distribution<>{1.0f64 - strength, strength}(*generator))
                        param += std::normal_distribution<Scalar>(0, 1)(*generator);
            }
        }

        void Mutate(const float64 &strength, std::ranlux48_base *generator)
        {
            Mutate(Serialize(), strength, generator);
        }

        template <size_t I = 0>
        void Cross(const auto params_a, const auto params_b, auto params_c, std::ranlux48_base *generator) const
        {
            auto &m_a = std::get<I>(params_a);
            auto &m_b = std::get<I>(params_b);
            auto &m_c = std::get<I>(params_c);

            if constexpr (I + 1 != std::tuple_size<SerializeT>{})
            {
                for (auto &&[a, b, c] : std::views::zip(m_a.reshaped(), m_b.reshaped(), m_c.reshaped()))
                {
                    if (std ::discrete_distribution<>{1, 1}(*generator))
                        c = a;
                    else
                        c = b;
                }

                Cross<I + 1>(params_a, params_b, params_c, generator);
            }
            if constexpr (I + 1 == std::tuple_size<SerializeT>{})
            {
                for (auto &&[a, b, c] : std::views::zip(m_a.reshaped(), m_b.reshaped(), m_c.reshaped()))
                {
                    if (std ::discrete_distribution<>{1, 1}(*generator))
                        c = a;
                    else
                        c = b;
                }
            }
        }

        Layer Cross(const Layer &other, std::ranlux48_base *generator) const
        {
            Layer layer;
            Cross(Serialize(), other.Serialize(), layer.Serialize(), generator);
            return layer;
        }

        const auto Serialize() const
        {
            return std::tie(w_f, w_i, w_o, w_c, u_f, u_i, u_o, u_c, b_f, b_i, b_o, b_c);
        }

        auto Serialize()
        {
            return std::tie(w_f, w_i, w_o, w_c, u_f, u_i, u_o, u_c, b_f, b_i, b_o, b_c);
        }
    };
};

template <IToVec State, IFromVec Move, template <int32> typename OutputLayer = LinearLayer, typename... Layers>
requires std::is_same_v<typename State::ScalarT, typename Move::ScalarT>
class Agent : public BaseAgent<State, Move>
{
    using ScalarT = State::ScalarT;

    template <typename Tuple, int32 Inputs, size_t I = 0>
    static auto Initialize(std::ranlux48_base *generator)
    {
        using ElemType = std::tuple_element_t<I, Tuple>;
        using LayerType = typename ElemType::Layer<ScalarT, Inputs>;
        if constexpr (I + 1 != std::tuple_size<Tuple>{})
        {
            return std::tuple_cat(std::tuple{LayerType(generator)}, Initialize<Tuple, ElemType::NeuronsI, I + 1>(generator));
        }
        if constexpr (I + 1 == std::tuple_size<Tuple>{})
        {
            return std::tuple{LayerType(generator)};
        }
    }

    template <typename... Types>
    using tuple_cat_t = decltype(std::tuple_cat(std::declval<Types>()...));

    using TupleT = tuple_cat_t<std::tuple<Layers...>, std::tuple<OutputLayer<Move::Length>>>;
    using LayersT = std::invoke_result_t<decltype(Initialize<TupleT, State::Length>), std::ranlux48_base *>;

    template <int32 Inputs, size_t I = 0>
    static auto Process(LayersT &layers, const Eigen::Vector<ScalarT, Inputs> &input)
    {
        if constexpr (I + 1 != std::tuple_size<LayersT>{})
        {
            return Process<std::tuple_element_t<I, TupleT>::NeuronsI, I + 1>(layers, std::get<I>(layers)(input));
        }
        if constexpr (I + 1 == std::tuple_size<LayersT>{})
        {
            return std::get<I>(layers)(input);
        }
    }

    template <size_t I = 0>
    static void Reset(LayersT &layers)
    {
        if constexpr (I + 1 != std::tuple_size<LayersT>{})
        {
            std::get<I>(layers).Reset();
            Reset<I + 1>(layers);
        }
        if constexpr (I + 1 == std::tuple_size<LayersT>{})
        {
            std::get<I>(layers).Reset();
        }
    }

    template <typename T, size_t I = 0>
    static void Mutate(T &layers, const float64 &strength, std::ranlux48_base *generator)
    {
        if constexpr (I + 1 != std::tuple_size<T>{})
        {
            std::get<I>(layers).Mutate(strength, generator);
            Mutate<T, I + 1>(layers, strength, generator);
        }
        if constexpr (I + 1 == std::tuple_size<T>{})
        {
            std::get<I>(layers).Mutate(strength, generator);
        }
    }

    template <typename T, size_t I = 0>
    static auto Cross(const T &layers_a, const T &layers_b, std::ranlux48_base *generator)
    {
        if constexpr (I + 1 != std::tuple_size<T>{})
        {
            auto head = std::tuple{std::get<I>(layers_a).Cross(std::get<I>(layers_b), generator)};
            auto tail = Cross<T, I + 1>(layers_a, layers_b, generator);
            return std::tuple_cat(head, tail);
        }
        if constexpr (I + 1 == std::tuple_size<T>{})
        {
            return std::tuple{std::get<I>(layers_a).Cross(std::get<I>(layers_b), generator)};
        }
    }

    LayersT layers;

  public:
    Agent(std::ranlux48_base *generator) : BaseAgent<State, Move>(generator)
    {
        layers = Initialize<TupleT, State::Length>(generator);
    }

    Agent(std::ranlux48_base *generator, const LayersT &layers) : BaseAgent<State, Move>(generator), layers(layers) {}

    Agent(const Agent *other) : BaseAgent<State, Move>(other->generator), layers(other->layers) {}

    ~Agent() {}

    Move Process(const State &state)
    {
        return Move(Process<State::Length>(layers, state.ToVec()));
    }

    void Reset()
    {
        Reset(layers);
    }

    void Mutate(const float64 &strength)
    {
        Mutate<LayersT>(layers, strength, this->generator);
    }

    Agent *Cross(const Agent *other) const
    {
        return new Agent(this->generator, Cross<LayersT>(layers, other->layers, this->generator));
    }

    std::vector<byte> Save() const
    {
        return Serializer::Save<LayersT>(layers);
    }

    void Load(const byte *data)
    {
        layers = Serializer::Load<LayersT>(data);
    }
};

} // namespace Neural
