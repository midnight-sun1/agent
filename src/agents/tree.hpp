#pragma once
#include "base.hpp"
#include "Eigen/Dense"
#include "serializer.hpp"

namespace Tree
{
template <typename Scalar, int32 Length>
struct Node
{
    Node *left, *right;
    bool (*op)(Node *node, const Eigen::Vector<Scalar, Length> &);
    size_t action;
    int32 index;
    int32 op_n;

    using BuildT = std::tuple<Node *, Node *, size_t, int32, int32>;

    const auto Serialize() const
    {
        return std::tie(left, right, action, index, op_n);
    }

    void Build(const BuildT &param)
    {
        left = std::get<0>(param);
        right = std::get<1>(param);
        action = std::get<2>(param);
        index = std::get<3>(param);
        op_n = std::get<4>(param);

        if (not left or not right)
            return;

        if (op_n == 0)
            op = [](Node *node, const Eigen::Vector<Scalar, Length> &state) { return state[node->index] < 0; };
        else if (op_n == 1)
            op = [](Node *node, const Eigen::Vector<Scalar, Length> &state) { return state[node->index] > 0; };
        else
            op = [](Node *node, const Eigen::Vector<Scalar, Length> &state) { return state[node->index] == 0; };
    }
};

template <IToVec State, IFromReinforced Move, int32 MaxDepth = 8>
class Agent : public BaseAgent<State, Move>
{
    using ScalarT = State::ScalarT;
    static constexpr size_t SLength = State::Length;
    static constexpr size_t MDomain = Move::Length;

    using NodeT = Node<ScalarT, SLength>;

    NodeT *root;

    void Delete(NodeT *node)
    {
        if (node->left)
            Delete(node->left);
        if (node->right)
            Delete(node->right);

        delete node;
    }

    Move Process(NodeT *node, const Eigen::Vector<ScalarT, SLength> &state)
    {
        if (node->left and not node->op(node, state))
            return Process(node->left, state);
        if (node->right and node->op(node, state))
            return Process(node->right, state);

        return Move(node->action);
    }

    void Mutate(NodeT *node, const float64 &alpha, int32 depth = 0)
    {
        if (node->op)
            if (std::uniform_real_distribution<float64>(0, 1)(*this->generator) <= alpha)
            {
                if (std::discrete_distribution<>{1, 1}(*this->generator))
                    return Mutate(node->left, alpha, depth + 1);
                else
                    return Mutate(node->right, alpha, depth + 1);
            }

        if (std::uniform_real_distribution<float64>(0, 1)(*this->generator) > alpha) // Mutate subtree
        {
            if (std::uniform_real_distribution<float64>(0, 1)(*this->generator) > alpha and depth < MaxDepth) // Create node
            {
                if (node->op)
                {
                    Delete(node->left);
                    Delete(node->right);
                }
                node->index = std::uniform_int_distribution<>(0, SLength - 1)(*this->generator);
                node->op_n = std::uniform_int_distribution<>(0, 2)(*this->generator);

                if (node->op_n == 0)
                    node->op = [](NodeT *node, const Eigen::Vector<ScalarT, SLength> &state) { return state[node->index] < 0; };
                else if (node->op_n == 1)
                    node->op = [](NodeT *node, const Eigen::Vector<ScalarT, SLength> &state) { return state[node->index] > 0; };
                else
                    node->op = [](NodeT *node, const Eigen::Vector<ScalarT, SLength> &state) { return state[node->index] == 0; };

                node->left = new NodeT{
                    .left = nullptr,
                    .right = nullptr,
                    .op = nullptr,
                    .action = std::uniform_int_distribution<size_t>(0, static_cast<int32>(MDomain) - 1)(*this->generator),
                };
                node->right = new NodeT{
                    .left = nullptr,
                    .right = nullptr,
                    .op = nullptr,
                    .action = std::uniform_int_distribution<size_t>(0, static_cast<int32>(MDomain) - 1)(*this->generator),
                };

                Mutate(node->left, alpha, depth + 1);
                Mutate(node->right, alpha, depth + 1);
            }
            else // Create leaf
            {
                if (node->op)
                {
                    Delete(node->left);
                    Delete(node->right);
                    node->left = nullptr;
                    node->right = nullptr;
                    node->op = nullptr;
                }
                node->action = std::uniform_int_distribution(0, static_cast<int32>(MDomain) - 1)(*this->generator);
            }
        }
    }

    NodeT *Clone(const NodeT *other) const
    {
        if (other->op)
            return new NodeT{
                .left = Clone(other->left),
                .right = Clone(other->right),
                .op = other->op,
                .index = other->index,
                .op_n = other->op_n,
            };

        return new NodeT{
            .left = nullptr,
            .right = nullptr,
            .op = nullptr,
            .action = other->action,
        };
    }

  public:
    Agent(std::ranlux48_base *generator) : BaseAgent<State, Move>(generator)
    {
        root = new NodeT{
            .left = nullptr,
            .right = nullptr,
            .op = nullptr,
            .action = std::uniform_int_distribution<size_t>(0, static_cast<int32>(MDomain) - 1)(*this->generator),
        };
        Mutate(root, 0);
    }

    Agent(std::ranlux48_base *generator, NodeT *node) : BaseAgent<State, Move>(generator), root(node) {}

    Agent(const Agent *other) : BaseAgent<State, Move>(other->generator)
    {
        root = Clone(other->root);
    }

    ~Agent()
    {
        Delete(root);
    }

    Move Process(const State &state)
    {
        return Process(root, state.ToVec());
    }

    void Reset() {}

    void Mutate(const float64 &strength)
    {
        Mutate(root, std::pow(1.0f64 - strength, 1.0f64 / MaxDepth));
    }

    Agent *Cross(const Agent *other) const
    {
        if (not other->root->op)
            return new Agent(this->generator, Clone(root));
        else if (not root->op)
            return new Agent(this->generator, Clone(other->root));

        NodeT *newroot = new NodeT;
        if (std ::discrete_distribution<>{1, 1}(*this->generator))
        {
            newroot->left = Clone(root->left);
            newroot->right = Clone(other->root->right);
            newroot->op = root->op;
            newroot->index = root->index;
            newroot->action = root->action;
            newroot->op_n = root->op_n;
        }
        else
        {
            newroot->left = Clone(other->root->left);
            newroot->right = Clone(root->right);
            newroot->op = other->root->op;
            newroot->index = other->root->index;
            newroot->action = other->root->action;
            newroot->op_n = other->root->op_n;
        }
        return new Agent(this->generator, newroot);
    }

    std::vector<byte> Save() const
    {
        return Serializer::Save<NodeT *>(root);
    }

    void Load(const byte *data)
    {
        Delete(root);
        root = Serializer::Load<NodeT *>(data);
    }
};
} // namespace Tree
