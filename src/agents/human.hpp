#pragma once
#include <random>
#include "base.hpp"

namespace Human
{
template <typename State, IFromKeyboard Move>
class Agent : public BaseAgent<State, Move>
{
  public:
    Agent(std::ranlux48_base *generator) : BaseAgent<State, Move>(generator) {}

    ~Agent() {}

    Move Process(const State &state)
    {
        return Move(this->keys);
    }

    void Reset() {}
};
} // namespace Human
