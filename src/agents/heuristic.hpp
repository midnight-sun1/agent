#pragma once
#include <random>
#include "base.hpp"

namespace Heuristic
{
template <typename State, typename Move>
class Agent : public BaseAgent<State, Move>
{
  public:
    Agent(std::ranlux48_base *generator) : BaseAgent<State, Move>(generator) {}

    ~Agent() {}

    Move Process(const State &state)
    {
        return Move::template Heuristic<State>(this->generator, state);
    }

    void Reset() {}
};
} // namespace Heuristic
