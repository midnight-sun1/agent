#pragma once
#include <random>
#include "base.hpp"

namespace Dummy
{
template <typename State, typename Move>
class Agent : public BaseAgent<State, Move>
{
  public:
    Agent(std::ranlux48_base *generator) : BaseAgent<State, Move>(generator) {}

    ~Agent() {}

    Move Process(const State &state)
    {
        return Move();
    }

    void Reset() {}
};
} // namespace Dummy
