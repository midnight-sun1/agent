#pragma once
#include <random>
#include "base.hpp"

namespace Random
{
template <typename State, IFromGenerator Move>
class Agent : public BaseAgent<State, Move>
{
  public:
    Agent(std::ranlux48_base *generator) : BaseAgent<State, Move>(generator) {}

    ~Agent() {}

    Move Process(const State &state)
    {
        return Move(this->generator);
    }

    void Reset() {}
};
} // namespace Random
