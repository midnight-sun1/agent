#pragma once
#include <cmath>
#include <random>
#include "base.hpp"
#include "serializer.hpp"

namespace Reinforced
{
template <IToReinforced State, IFromReinforced Move, bool Learning = true, float64 Epsilon = 1.0f64, float64 Alpha = 0.1f64, float64 Gamma = 0.99f64, float64 Lambda = 0.0001f64, float64 MinEpsilon = 0.0f64>
class Agent : public BaseAgent<State, Move>
{
    using ScalarT = State::ScalarT;

    Eigen::Matrix<ScalarT, State::RDomain, Move::MDomain> q_table;
    size_t prev_state;
    size_t prev_action;
    float64 epsilon;
    int64 tick;

  public:
    Agent(std::ranlux48_base *generator) : BaseAgent<State, Move>(generator)
    {
        q_table = Eigen::Matrix<ScalarT, State::RDomain, Move::MDomain>::Zero();
        epsilon = Epsilon;
        tick = -1;
    }

    Agent(std::ranlux48_base *generator, float64 epsilon) : BaseAgent<State, Move>(generator), epsilon(epsilon)
    {
        q_table = Eigen::Matrix<ScalarT, State::RDomain, Move::MDomain>::Zero();
        tick = -1;
    }

    ~Agent() {}

    Move Process(const State &state)
    {
        if (state.RDone())
            return Move();

        auto current_state = state.RIndex();
        auto reward = state.RValue();
        auto max = [](const auto &vec) { return *std::max_element(vec.begin(), vec.end()); };
        auto argmax = [](const auto &vec) { return std::distance(vec.begin(), std::max_element(vec.begin(), vec.end())); };

        if constexpr (Learning)
        {
            q_table(prev_state, static_cast<int32>(prev_action)) = (ScalarT)((1 - Alpha) * q_table(prev_state, static_cast<int32>(prev_action)) + Alpha * (reward + Gamma * max(q_table.row(current_state))));
            size_t action;

            if (std::uniform_real_distribution(0.0, 1.0)(*this->generator) < epsilon or max(q_table.row(current_state)) == static_cast<ScalarT>(0))
                action = std::uniform_int_distribution(0, static_cast<int32>(Move::MDomain) - 1)(*this->generator);
            else
                action = argmax(q_table.row(current_state));

            prev_state = current_state;
            prev_action = action;
            epsilon = std::max(MinEpsilon, Epsilon * std::exp(-Lambda * tick));

            return Move(action);
        }
        if constexpr (not Learning)
        {
            if (max(q_table.row(current_state)) == static_cast<ScalarT>(0))
                return Move(std::uniform_int_distribution(0, static_cast<int32>(Move::MDomain) - 1)(*this->generator));
            return Move(argmax(q_table.row(current_state)));
        }
    }

    void Reset()
    {
        tick++;
        prev_state = 0;
        prev_action = 0;
    }

    std::vector<byte> Save() const
    {
        return Serializer::Save<decltype(q_table)>(q_table);
    }

    void Load(const byte *data)
    {
        q_table = Serializer::Load<decltype(q_table)>(data);
    }
};
} // namespace Reinforced
