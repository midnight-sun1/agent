FROM archlinux
RUN pacman -Syu --noconfirm; pacman -S git cmake ninja gcc intel-oneapi-mkl xorg openal libogg libvorbis flac mold --noconfirm
RUN git clone https://gitlab.com/midnight-sun1/agent
RUN git -C agent submodule update --init --recursive
